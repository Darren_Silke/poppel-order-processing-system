﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Order_Processing_System.Domain_Layer
{
    public class Customer
    {
        private string idNumber;
        private string name;
        private string surname;
        private string emailAddress;
        private string deliveryAddress;
        private string contactNumber;
        private bool blacklisted;

        public string IDNumber
        {
            get
            {
                return idNumber;
            }
            set
            {
                idNumber = value;
            }
        }

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public string Surname
        {
            get
            {
                return surname;
            }
            set
            {
                surname = value;
            }
        }

        public string EmailAddress
        {
            get
            {
                return emailAddress;
            }
            set
            {
                emailAddress = value;
            }
        }

        public string DeliveryAddress
        {
            get
            {
                return deliveryAddress;
            }
            set
            {
                deliveryAddress = value;
            }
        }

        public string ContactNumber
        {
            get
            {
                return contactNumber;
            }
            set
            {
                contactNumber = value;
            }
        }

        public bool Blacklisted
        {
            get
            {
                return blacklisted;
            }
            set
            {
                blacklisted = value;
            }
        }
    }
}
