﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Order_Processing_System.Domain_Layer
{
    public class StockItem : IComparable
    {
        private string productCode;
        private string description;
        private DateTime expiryDate;
        private int unitsInStock;
        private decimal pricePerUnit;

        public StockItem()
        {
            expiryDate = new DateTime();
        }

        public string ProductCode
        {
            get
            {
                return productCode;
            }
            set
            {
                productCode = value;
            }
        }

        public string Description
        {
            get
            {
                return description;
            }
            set
            {
                description = value;
            }
        }

        public DateTime ExpiryDate
        {
            get
            {
                return expiryDate;
            }
            set
            {
                expiryDate = value;
            }
        }

        public int UnitsInStock
        {
            get
            {
                return unitsInStock;
            }
            set
            {
                unitsInStock = value;
            }
        }

        public decimal PricePerUnit
        {
            get
            {
                return pricePerUnit;
            }
            set
            {
                pricePerUnit = value;
            }
        }

        public int Compare(object lhs, object rhs)
        {

            StockItem lhsStockItem = (StockItem)lhs;
            StockItem rhsStockItem = (StockItem)rhs;

            if (lhsStockItem.ProductCode.CompareTo(rhsStockItem.ProductCode.ToUpper()) < 0)
            {
                return -1;
            }
            else if (lhsStockItem.ProductCode.CompareTo(rhsStockItem.ProductCode.ToUpper()) > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public int CompareTo(object rhs)
        {
            return Compare(this, rhs);
        }
    }
}
