﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Order_Processing_System.Domain_Layer
{
    public class OrderItem
    {
        private string customerIDNumber;
        private string itemProductCode;
        private int itemQuantity;
        private bool orderProcessed;

        public string CustomerIDNumber
        {
            get
            {
                return customerIDNumber;
            }
            set
            {
                customerIDNumber = value;
            }
        }

        public string ItemProductCode
        {
            get
            {
                return itemProductCode;
            }
            set
            {
                itemProductCode = value;
            }
        }

        public int ItemQuantity
        {
            get
            {
                return itemQuantity;
            }
            set
            {
                itemQuantity = value;
            }
        }

        public bool OrderProcessed
        {
            get
            {
                return orderProcessed;
            }
            set
            {
                orderProcessed = value;
            }
        }
    }
}
