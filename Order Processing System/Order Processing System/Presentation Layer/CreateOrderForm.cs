﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Order_Processing_System.Database_Layer;
using Order_Processing_System.Domain_Layer;
using Order_Processing_System.Application_Logic_Layer;

namespace Order_Processing_System.Presentation_Layer
{
    public partial class CreateOrderForm : Form
    {
        private Customer customer;
        private Collection<StockItem> stockItemsFromDatabase;
        public delegate void ReturnToMainFormButtonClickEventHandler(Button button);
        public event ReturnToMainFormButtonClickEventHandler ReturnToMainFormButtonClick;

        public CreateOrderForm()
        {
            InitializeComponent();
            SetupForm();
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);
            if (e.CloseReason == CloseReason.WindowsShutDown) return;
            // Confirm that the user wishes to exit the application.
            switch (MessageBox.Show(this, "Are you sure you want to exit the application?", "Exit Application", MessageBoxButtons.YesNo))
            {
                case DialogResult.No:
                    e.Cancel = true;
                    break;

                default:
                    break;
            }
        }

        private void SetupItemDetailsListView()
        {
            itemDetailsListView.View = View.Details;

            // Set up columns of ListView.
            itemDetailsListView.Columns.Insert(0, "Product Code", 298, HorizontalAlignment.Left);
            itemDetailsListView.Columns.Insert(1, "Description", 298, HorizontalAlignment.Left);
            itemDetailsListView.Columns.Insert(2, "Expiry Date", 298, HorizontalAlignment.Left);
            itemDetailsListView.Columns.Insert(3, "Units In Stock", 299, HorizontalAlignment.Left);
            itemDetailsListView.Columns.Insert(4, "Price Per Unit (R)", 300, HorizontalAlignment.Left);

        }

        private void AddInitialItemDetailsToItemDetailsListView()
        {
            stockItemsFromDatabase = new StockItemController().GetStockDetails(false);
            ListViewItem itemDetails;

            foreach (StockItem stockItem in stockItemsFromDatabase)
            {
                itemDetails = new ListViewItem();
                itemDetails.Text = stockItem.ProductCode;
                itemDetails.SubItems.Add(stockItem.Description);
                itemDetails.SubItems.Add(stockItem.ExpiryDate.ToString("d MMMM yyyy"));
                itemDetails.SubItems.Add(stockItem.UnitsInStock.ToString());
                itemDetails.SubItems.Add(stockItem.PricePerUnit.ToString());

                itemDetailsListView.Items.Add(itemDetails);
            }
        }

        private void SetupOrderDetailsListView()
        {
            orderDetailsListView.View = View.Details;

            // Set up columns of ListView.
            orderDetailsListView.Columns.Insert(0, "Product Code", 298, HorizontalAlignment.Left);
            orderDetailsListView.Columns.Insert(1, "Description", 298, HorizontalAlignment.Left);
            orderDetailsListView.Columns.Insert(2, "Quantity", 298, HorizontalAlignment.Left);
            orderDetailsListView.Columns.Insert(3, "Price Per Unit (R)", 299, HorizontalAlignment.Left);
            orderDetailsListView.Columns.Insert(4, "Sub Total (R)", 300, HorizontalAlignment.Left);

        }

        private void submitButton_Click(object sender, EventArgs e)
        {
            customer = new CustomerController().GetCustomerDetails(idNumberTextBox.Text);

            if(customer.IDNumber != null)
            {
                nameTextBox.Text = customer.Name;
                surnameTextBox.Text = customer.Surname;
                emailAddressTextBox.Text = customer.EmailAddress;
                deliveryAddressTextBox.Text = customer.DeliveryAddress;
                contactNumberTextBox.Text = customer.ContactNumber;

                if (customer.Blacklisted == false)
                {
                    submitButton.Enabled = false;
                    idNumberTextBox.ReadOnly = true;
                    customerStatusTextBox.BackColor = Color.FromKnownColor(KnownColor.Control);
                    customerStatusTextBox.ForeColor = System.Drawing.SystemColors.WindowText;
                    customerStatusTextBox.Text = "APPROVED";
                    searchTextBox.Enabled = true;
                    itemDetailsListView.Enabled = true;
                    SetupItemDetailsListView();
                    AddInitialItemDetailsToItemDetailsListView();
                    orderDetailsListView.Enabled = true;
                    SetupOrderDetailsListView();
                }
                else
                {
                    customerStatusTextBox.BackColor = Color.FromKnownColor(KnownColor.Control);
                    customerStatusTextBox.ForeColor = Color.Red;
                    customerStatusTextBox.Text = "BLACKLISTED";
                    MessageBox.Show("Customer is blacklisted. Enter different ID number.", "Customer Blacklisted", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                }
            }
            else
            {
                nameTextBox.Text = "";
                surnameTextBox.Text = "";
                emailAddressTextBox.Text = "";
                deliveryAddressTextBox.Text = "";
                contactNumberTextBox.Text = "";
                customerStatusTextBox.Text = "";
                MessageBox.Show("Customer does not exist.", "Invalid ID Number", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void idNumberTextBox_TextChanged(object sender, EventArgs e)
        {
            if(idNumberTextBox.Text == "")
            {
                submitButton.Enabled = false;
            }
            else
            {
                submitButton.Enabled = true;
            }
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            switch (MessageBox.Show("Are you sure you want to cancel the order?", "Cancel Order", MessageBoxButtons.YesNo))
            {
                case DialogResult.No:
                    break;

                case DialogResult.Yes:                             
                    OnReturnToMainFormButtonClick((Button)sender);
                    this.Visible = false;
                    break;
            }
        }

        private void OnReturnToMainFormButtonClick(Button button)
        {
            if (ReturnToMainFormButtonClick != null)
            {
                SetupForm();
                ReturnToMainFormButtonClick(button);
            }
        }

        private void SetupForm()
        {
            idNumberTextBox.ReadOnly = false;
            idNumberTextBox.Text = "";
            this.VerticalScroll.Value = 0;
            idNumberTextBox.Focus();
            nameTextBox.Text = "";
            surnameTextBox.Text = "";
            emailAddressTextBox.Text = "";
            deliveryAddressTextBox.Text = "";
            contactNumberTextBox.Text = "";
            customerStatusTextBox.Text = "";
            submitButton.Enabled = false;
            searchTextBox.Enabled = false;
            searchTextBox.Text = "";
            searchButton.Enabled = false;
            quantityNumericUpDown.Enabled = false;
            quantityNumericUpDown.Value = 1;
            itemDetailsListView.Enabled = false;
            itemDetailsListView.Clear();
            addItemButton.Enabled = false;
            orderDetailsListView.Enabled = false;
            orderDetailsListView.Clear();
            removeItemButton.Enabled = false;
            totalTextBox.Text = "";
            submitOrderButton.Enabled = false;
            this.Refresh();
        }

        private void searchTextBox_TextChanged(object sender, EventArgs e)
        {
            if (searchTextBox.Text == "")
            {
                itemDetailsListView.Clear();
                SetupItemDetailsListView();
                AddInitialItemDetailsToItemDetailsListView();
                searchButton.Enabled = false;
            }
            else
            {
                searchButton.Enabled = true;
            }
        }

        private void itemDetailsListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (itemDetailsListView.SelectedIndices.Count == 0)
            {
                addItemButton.Enabled = false;
                quantityNumericUpDown.Enabled = false;
            }
            else
            {
                quantityNumericUpDown.Enabled = true;
                addItemButton.Enabled = true;
            }
        }

        private void addItemButton_Click(object sender, EventArgs e)
        {
            quantityNumericUpDown.BackColor = System.Drawing.SystemColors.Window;
            decimal newUnitsInStock = Convert.ToDecimal(itemDetailsListView.SelectedItems[0].SubItems[3].Text) - quantityNumericUpDown.Value;
                        
            if(itemDetailsListView.SelectedItems[0].SubItems[3].Text == "0")
            {
                MessageBox.Show("The item selected is currently out of stock.", "Out Of Stock", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                quantityNumericUpDown.Value = 1;
            }
            else if(newUnitsInStock < 0)
            {
                quantityNumericUpDown.BackColor = Color.Red;
                MessageBox.Show("Check stock availability and change quantity.", "Insuffient Stock", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                bool found = false;
                int i = 0;
                while ((found == false) && (i < orderDetailsListView.Items.Count))
                {
                    if (itemDetailsListView.SelectedItems[0].SubItems[0].Text == orderDetailsListView.Items[i].SubItems[0].Text)
                    {
                        decimal newQuantity = Convert.ToDecimal(orderDetailsListView.Items[i].SubItems[2].Text) + quantityNumericUpDown.Value;
                        orderDetailsListView.Items[i].SubItems[2].Text = newQuantity.ToString();
                        decimal newSubTotal = newQuantity * Convert.ToDecimal(itemDetailsListView.SelectedItems[0].SubItems[4].Text);
                        orderDetailsListView.Items[i].SubItems[4].Text = newSubTotal.ToString();
                        found = true;
                    }
                    i++;
                }

                if (found == false)
                {
                    decimal subTotal = quantityNumericUpDown.Value * Convert.ToDecimal(itemDetailsListView.SelectedItems[0].SubItems[4].Text);
                    ListViewItem orderItemDetails = new ListViewItem();
                    orderItemDetails.Text = itemDetailsListView.SelectedItems[0].SubItems[0].Text;
                    orderItemDetails.SubItems.Add(itemDetailsListView.SelectedItems[0].SubItems[1]);
                    orderItemDetails.SubItems.Add(quantityNumericUpDown.Value.ToString());
                    orderItemDetails.SubItems.Add(itemDetailsListView.SelectedItems[0].SubItems[4]);
                    orderItemDetails.SubItems.Add(subTotal.ToString());
                    orderDetailsListView.Items.Add(orderItemDetails);
                }
                decimal total = 0;

                for (i = 0; i < orderDetailsListView.Items.Count; i++)
                {
                    total += Convert.ToDecimal(orderDetailsListView.Items[i].SubItems[4].Text);
                }
                totalTextBox.Text = total.ToString();
                itemDetailsListView.SelectedItems[0].SubItems[3].Text = newUnitsInStock.ToString();
                quantityNumericUpDown.Value = 1;
                submitOrderButton.Enabled = true;
            }
        }

        private void orderDetailsListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (orderDetailsListView.SelectedIndices.Count == 0)
            {
                removeItemButton.Enabled = false;
            }
            else
            {
                removeItemButton.Enabled = true;
            }
        }

        private void removeItemButton_Click(object sender, EventArgs e)
        {
            int i = 0;
            bool found = false;
            decimal total = 0; ;
            while((found == false) && (i < itemDetailsListView.Items.Count))
            {
                if(orderDetailsListView.SelectedItems[0].SubItems[0].Text == itemDetailsListView.Items[i].SubItems[0].Text)
                {                
                    int newUnitsInStock = Convert.ToInt32(orderDetailsListView.SelectedItems[0].SubItems[2].Text) + Convert.ToInt32(itemDetailsListView.Items[i].SubItems[3].Text);
                    itemDetailsListView.Items[i].SubItems[3].Text = newUnitsInStock.ToString();
                    total = Convert.ToDecimal(totalTextBox.Text) - Convert.ToDecimal(orderDetailsListView.SelectedItems[0].SubItems[4].Text);
                    orderDetailsListView.SelectedItems[0].Remove();
                    found = true;
                }
                i++;
            }

            if(total == 0)
            {
                totalTextBox.Text = "";
            }
            else
            {
                totalTextBox.Text = total.ToString();
            }

            if(orderDetailsListView.Items.Count == 0)
            {
                submitOrderButton.Enabled = false;
            }
            orderDetailsListView.Focus();
        }

        private void searchButton_Click(object sender, EventArgs e)
        {
            string searchString = searchTextBox.Text;
            string[] descriptions = new string[itemDetailsListView.Items.Count];
            StockItem stockItem;
            ArrayList stockItems = new ArrayList();
            ArrayList newStockItems;
            ListViewItem newItemDetails;

            for(int i = 0; i < itemDetailsListView.Items.Count; i++)
            {
                stockItem = new StockItem();
                stockItem.ProductCode = itemDetailsListView.Items[i].SubItems[0].Text;
                stockItem.Description = itemDetailsListView.Items[i].SubItems[1].Text;
                stockItem.ExpiryDate = Convert.ToDateTime(itemDetailsListView.Items[i].SubItems[2].Text);
                stockItem.UnitsInStock = Convert.ToInt32(itemDetailsListView.Items[i].SubItems[3].Text);
                stockItem.PricePerUnit = Convert.ToDecimal(itemDetailsListView.Items[i].SubItems[4].Text);

                stockItems.Add(stockItem);
            }

            newStockItems = Search.FindStockItemMatches(searchString, stockItems);
            newStockItems.Sort();
            itemDetailsListView.Clear();
            SetupItemDetailsListView();

            foreach(StockItem newStockItem in newStockItems)
            {
                newItemDetails = new ListViewItem();
                newItemDetails.Text = newStockItem.ProductCode;
                newItemDetails.SubItems.Add(newStockItem.Description);
                newItemDetails.SubItems.Add(newStockItem.ExpiryDate.ToString("d MMMM yyyy"));
                newItemDetails.SubItems.Add(newStockItem.UnitsInStock.ToString());
                newItemDetails.SubItems.Add(newStockItem.PricePerUnit.ToString());

                itemDetailsListView.Items.Add(newItemDetails);
            }
        }

        private void submitOrderButton_Click(object sender, EventArgs e)
        {
            switch(MessageBox.Show("Are you sure you want to submit the order?", "Submit Order", MessageBoxButtons.YesNo))
            {
                case DialogResult.No:
                    break;

                case DialogResult.Yes:

                    OrderItem orderItem;
                    Collection<OrderItem> orderItems = new Collection<OrderItem>();
                    OrderItemController orderItemController = new OrderItemController();

                    for (int i = 0; i < orderDetailsListView.Items.Count; i++)
                    {
                        orderItem = new OrderItem();
                        orderItem.CustomerIDNumber = idNumberTextBox.Text;
                        orderItem.ItemProductCode = orderDetailsListView.Items[i].SubItems[0].Text;
                        orderItem.ItemQuantity = Convert.ToInt32(orderDetailsListView.Items[i].SubItems[2].Text);
                        orderItem.OrderProcessed = false;
                        orderItems.Add(orderItem);
                    }

                    if (orderItemController.AddOrderDetails(orderItems) == true)
                    {
                        MessageBox.Show("Order submitted successfully.", "Submit Order", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        OnReturnToMainFormButtonClick((Button)sender);
                        this.Visible = false;
                    }
                    break;
            }
        }
    }
}
