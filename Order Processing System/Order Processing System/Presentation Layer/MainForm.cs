﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Order_Processing_System.Presentation_Layer;

namespace Order_Processing_System
{
    public partial class MainForm : Form
    {
        private CreateCustomerForm createCustomerForm;
        private CreateOrderForm createOrderForm;
        private ViewPickingListForm viewPickingListForm;
        private ViewExpiredProductsForm viewExpiredProductsForm;
       
        public MainForm()
        {
            InitializeComponent();

            createCustomerForm = new CreateCustomerForm();
            createCustomerForm.FormClosed += new FormClosedEventHandler(CreateCustomerFormClosed); 
            createCustomerForm.ReturnToMainFormButtonClick += createCustomerForm_ReturnToMainFormButtonClick;

            createOrderForm = new CreateOrderForm();
            createOrderForm.FormClosed += new FormClosedEventHandler(CreateOrderFormClosed);
            createOrderForm.ReturnToMainFormButtonClick += createOrderForm_ReturnToMainFormButtonClick;

            viewPickingListForm = new ViewPickingListForm();
            viewPickingListForm.FormClosed += new FormClosedEventHandler(ViewPickingListFormClosed);
            viewPickingListForm.ReturnToMainFormButtonClick += viewPickingListForm_ReturnToMainFormButtonClick;


            viewExpiredProductsForm = new ViewExpiredProductsForm();
            viewExpiredProductsForm.FormClosed += new FormClosedEventHandler(ViewExpiredProductsFormClosed);
            viewExpiredProductsForm.ReturnToMainFormButtonClick += viewExpiredProductsForm_ReturnToMainFormButtonClick;
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);
            if (e.CloseReason == CloseReason.WindowsShutDown) return;
            // Confirm that the user wishes to exit the application.
            switch (MessageBox.Show(this, "Are you sure you want to exit the application?", "Exit Application", MessageBoxButtons.YesNo))
            {
                case DialogResult.No:
                    e.Cancel = true;
                    break;

                default:
                    break;
            }
        }

        private void CreateCustomerFormClosed(object sender, FormClosedEventArgs e)
        {
            System.Environment.Exit(0);
        }

        private void createCustomerButton_Click(object sender, EventArgs e)
        {
            createCustomerForm.Show();
            this.Visible = false;
        }
 
        private void createCustomerForm_ReturnToMainFormButtonClick(Button sender)
        {
            this.Visible = true;
        }

        private void CreateOrderFormClosed(object sender, FormClosedEventArgs e)
        {
            System.Environment.Exit(0);
        }

        private void createOrderButton_Click(object sender, EventArgs e)
        {
            createOrderForm.Show();
            this.Visible = false;
        }

        private void createOrderForm_ReturnToMainFormButtonClick(Button sender)
        {
            this.Visible = true;
        }

        private void ViewPickingListFormClosed(object sender, FormClosedEventArgs e)
        {
            System.Environment.Exit(0);
        }

        private void viewPickingListButton_Click(object sender, EventArgs e)
        {
            viewPickingListForm.Show();
            this.Visible = false;
        }

        private void viewPickingListForm_ReturnToMainFormButtonClick(Button sender)
        {
            this.Visible = true;
        }

        private void ViewExpiredProductsFormClosed(object sender, FormClosedEventArgs e)
        {
            System.Environment.Exit(0);
        }

        private void viewExpiredProductsButton_Click(object sender, EventArgs e)
        {
            viewExpiredProductsForm.Show();
            this.Visible = false;
        }

        private void viewExpiredProductsForm_ReturnToMainFormButtonClick(Button sender)
        {
            this.Visible = true;
        }
    }
}
