﻿namespace Order_Processing_System
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.poppelLogoPictureBox = new System.Windows.Forms.PictureBox();
            this.flowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.createCustomerButton = new System.Windows.Forms.Button();
            this.createOrderButton = new System.Windows.Forms.Button();
            this.viewPickingListButton = new System.Windows.Forms.Button();
            this.viewExpiredProductsButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.poppelLogoPictureBox)).BeginInit();
            this.flowLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // poppelLogoPictureBox
            // 
            this.poppelLogoPictureBox.Image = global::Order_Processing_System.Properties.Resources.Poppel_Logo;
            this.poppelLogoPictureBox.Location = new System.Drawing.Point(280, 12);
            this.poppelLogoPictureBox.Name = "poppelLogoPictureBox";
            this.poppelLogoPictureBox.Size = new System.Drawing.Size(311, 180);
            this.poppelLogoPictureBox.TabIndex = 0;
            this.poppelLogoPictureBox.TabStop = false;
            // 
            // flowLayoutPanel
            // 
            this.flowLayoutPanel.Controls.Add(this.createCustomerButton);
            this.flowLayoutPanel.Controls.Add(this.createOrderButton);
            this.flowLayoutPanel.Controls.Add(this.viewPickingListButton);
            this.flowLayoutPanel.Controls.Add(this.viewExpiredProductsButton);
            this.flowLayoutPanel.Location = new System.Drawing.Point(12, 235);
            this.flowLayoutPanel.Name = "flowLayoutPanel";
            this.flowLayoutPanel.Size = new System.Drawing.Size(825, 53);
            this.flowLayoutPanel.TabIndex = 0;
            // 
            // createCustomerButton
            // 
            this.createCustomerButton.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.createCustomerButton.Location = new System.Drawing.Point(3, 3);
            this.createCustomerButton.Margin = new System.Windows.Forms.Padding(3, 3, 38, 3);
            this.createCustomerButton.Name = "createCustomerButton";
            this.createCustomerButton.Size = new System.Drawing.Size(174, 46);
            this.createCustomerButton.TabIndex = 1;
            this.createCustomerButton.Text = "Create Customer";
            this.createCustomerButton.UseVisualStyleBackColor = true;
            this.createCustomerButton.Click += new System.EventHandler(this.createCustomerButton_Click);
            // 
            // createOrderButton
            // 
            this.createOrderButton.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.createOrderButton.Location = new System.Drawing.Point(218, 3);
            this.createOrderButton.Margin = new System.Windows.Forms.Padding(3, 3, 38, 3);
            this.createOrderButton.Name = "createOrderButton";
            this.createOrderButton.Size = new System.Drawing.Size(174, 46);
            this.createOrderButton.TabIndex = 2;
            this.createOrderButton.Text = "Create Order";
            this.createOrderButton.UseVisualStyleBackColor = true;
            this.createOrderButton.Click += new System.EventHandler(this.createOrderButton_Click);
            // 
            // viewPickingListButton
            // 
            this.viewPickingListButton.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.viewPickingListButton.Location = new System.Drawing.Point(433, 3);
            this.viewPickingListButton.Margin = new System.Windows.Forms.Padding(3, 3, 38, 3);
            this.viewPickingListButton.Name = "viewPickingListButton";
            this.viewPickingListButton.Size = new System.Drawing.Size(174, 46);
            this.viewPickingListButton.TabIndex = 3;
            this.viewPickingListButton.Text = "View Picking List";
            this.viewPickingListButton.UseVisualStyleBackColor = true;
            this.viewPickingListButton.Click += new System.EventHandler(this.viewPickingListButton_Click);
            // 
            // viewExpiredProductsButton
            // 
            this.viewExpiredProductsButton.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.viewExpiredProductsButton.Location = new System.Drawing.Point(648, 3);
            this.viewExpiredProductsButton.Name = "viewExpiredProductsButton";
            this.viewExpiredProductsButton.Size = new System.Drawing.Size(174, 46);
            this.viewExpiredProductsButton.TabIndex = 4;
            this.viewExpiredProductsButton.Text = "View Expired Products";
            this.viewExpiredProductsButton.UseVisualStyleBackColor = true;
            this.viewExpiredProductsButton.Click += new System.EventHandler(this.viewExpiredProductsButton_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(849, 300);
            this.Controls.Add(this.flowLayoutPanel);
            this.Controls.Add(this.poppelLogoPictureBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Main Form";
            ((System.ComponentModel.ISupportInitialize)(this.poppelLogoPictureBox)).EndInit();
            this.flowLayoutPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox poppelLogoPictureBox;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel;
        private System.Windows.Forms.Button createCustomerButton;
        private System.Windows.Forms.Button createOrderButton;
        private System.Windows.Forms.Button viewPickingListButton;
        private System.Windows.Forms.Button viewExpiredProductsButton;

    }
}

