﻿namespace Order_Processing_System.Presentation_Layer
{
    partial class ViewExpiredProductsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ViewExpiredProductsForm));
            this.poppelLogoPictureBox = new System.Windows.Forms.PictureBox();
            this.viewExpiredProductsLabel = new System.Windows.Forms.Label();
            this.itemDetailsLabel = new System.Windows.Forms.Label();
            this.searchTextBox = new System.Windows.Forms.TextBox();
            this.searchButton = new System.Windows.Forms.Button();
            this.itemDetailsListView = new System.Windows.Forms.ListView();
            this.backButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.poppelLogoPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // poppelLogoPictureBox
            // 
            this.poppelLogoPictureBox.Image = global::Order_Processing_System.Properties.Resources.Poppel_Logo;
            this.poppelLogoPictureBox.Location = new System.Drawing.Point(629, 12);
            this.poppelLogoPictureBox.Name = "poppelLogoPictureBox";
            this.poppelLogoPictureBox.Size = new System.Drawing.Size(311, 180);
            this.poppelLogoPictureBox.TabIndex = 2;
            this.poppelLogoPictureBox.TabStop = false;
            // 
            // viewExpiredProductsLabel
            // 
            this.viewExpiredProductsLabel.AutoSize = true;
            this.viewExpiredProductsLabel.Font = new System.Drawing.Font("Arial Narrow", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.viewExpiredProductsLabel.Location = new System.Drawing.Point(691, 209);
            this.viewExpiredProductsLabel.Name = "viewExpiredProductsLabel";
            this.viewExpiredProductsLabel.Size = new System.Drawing.Size(186, 24);
            this.viewExpiredProductsLabel.TabIndex = 3;
            this.viewExpiredProductsLabel.Text = "View Expired Products";
            // 
            // itemDetailsLabel
            // 
            this.itemDetailsLabel.AutoSize = true;
            this.itemDetailsLabel.Font = new System.Drawing.Font("Arial Narrow", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.itemDetailsLabel.Location = new System.Drawing.Point(12, 264);
            this.itemDetailsLabel.Name = "itemDetailsLabel";
            this.itemDetailsLabel.Size = new System.Drawing.Size(97, 23);
            this.itemDetailsLabel.TabIndex = 24;
            this.itemDetailsLabel.Text = "Item Details";
            // 
            // searchTextBox
            // 
            this.searchTextBox.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchTextBox.Location = new System.Drawing.Point(1280, 264);
            this.searchTextBox.Name = "searchTextBox";
            this.searchTextBox.Size = new System.Drawing.Size(160, 27);
            this.searchTextBox.TabIndex = 27;
            this.searchTextBox.TextChanged += new System.EventHandler(this.searchTextBox_TextChanged);
            // 
            // searchButton
            // 
            this.searchButton.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchButton.Location = new System.Drawing.Point(1446, 264);
            this.searchButton.Name = "searchButton";
            this.searchButton.Size = new System.Drawing.Size(84, 27);
            this.searchButton.TabIndex = 28;
            this.searchButton.Text = "Search";
            this.searchButton.UseVisualStyleBackColor = true;
            this.searchButton.Click += new System.EventHandler(this.searchButton_Click);
            // 
            // itemDetailsListView
            // 
            this.itemDetailsListView.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.itemDetailsListView.FullRowSelect = true;
            this.itemDetailsListView.GridLines = true;
            this.itemDetailsListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.itemDetailsListView.HideSelection = false;
            this.itemDetailsListView.Location = new System.Drawing.Point(12, 324);
            this.itemDetailsListView.MultiSelect = false;
            this.itemDetailsListView.Name = "itemDetailsListView";
            this.itemDetailsListView.Size = new System.Drawing.Size(1518, 162);
            this.itemDetailsListView.TabIndex = 29;
            this.itemDetailsListView.UseCompatibleStateImageBehavior = false;
            // 
            // backButton
            // 
            this.backButton.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.backButton.Location = new System.Drawing.Point(12, 546);
            this.backButton.Name = "backButton";
            this.backButton.Size = new System.Drawing.Size(174, 46);
            this.backButton.TabIndex = 39;
            this.backButton.Text = "Back";
            this.backButton.UseVisualStyleBackColor = true;
            this.backButton.Click += new System.EventHandler(this.backButton_Click);
            // 
            // ViewExpiredProductsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1546, 604);
            this.Controls.Add(this.backButton);
            this.Controls.Add(this.itemDetailsListView);
            this.Controls.Add(this.searchButton);
            this.Controls.Add(this.searchTextBox);
            this.Controls.Add(this.itemDetailsLabel);
            this.Controls.Add(this.viewExpiredProductsLabel);
            this.Controls.Add(this.poppelLogoPictureBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "ViewExpiredProductsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "View Expired Products Form";
            ((System.ComponentModel.ISupportInitialize)(this.poppelLogoPictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox poppelLogoPictureBox;
        private System.Windows.Forms.Label viewExpiredProductsLabel;
        private System.Windows.Forms.Label itemDetailsLabel;
        private System.Windows.Forms.TextBox searchTextBox;
        private System.Windows.Forms.Button searchButton;
        private System.Windows.Forms.ListView itemDetailsListView;
        private System.Windows.Forms.Button backButton;
    }
}