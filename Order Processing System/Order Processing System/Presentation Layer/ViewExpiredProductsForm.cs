﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Order_Processing_System.Domain_Layer;
using Order_Processing_System.Application_Logic_Layer;
using Order_Processing_System.Database_Layer;

namespace Order_Processing_System.Presentation_Layer
{
    public partial class ViewExpiredProductsForm : Form
    {
        private Collection<StockItem> stockItemsFromDatabase;
        public delegate void ReturnToMainFormButtonClickEventHandler(Button button);
        public event ReturnToMainFormButtonClickEventHandler ReturnToMainFormButtonClick;

        public ViewExpiredProductsForm()
        {
            InitializeComponent();
            SetupForm();
            SetupItemDetailsListView();
            AddInitialItemDetailsToItemDetailsListView();
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);
            if (e.CloseReason == CloseReason.WindowsShutDown) return;
            // Confirm that user wishes to exit the application.
            switch (MessageBox.Show(this, "Are you sure you want to exit the application?", "Exit Application", MessageBoxButtons.YesNo))
            {
                case DialogResult.No:
                    e.Cancel = true;
                    break;

                default:
                    break;
            }
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            OnReturnToMainFormButtonClick((Button)sender);
            this.Visible = false;
        }

        private void OnReturnToMainFormButtonClick(Button button)
        {
            if(ReturnToMainFormButtonClick != null)
            {
                SetupForm();
                ReturnToMainFormButtonClick(button);
            }
        }

        private void SetupItemDetailsListView()
        {
            itemDetailsListView.View = View.Details;

            // Set up columns of ListView. 
            itemDetailsListView.Columns.Insert(0, "Product Code", 298, HorizontalAlignment.Left);
            itemDetailsListView.Columns.Insert(1, "Description", 298, HorizontalAlignment.Left);
            itemDetailsListView.Columns.Insert(2, "Expiry Date", 298, HorizontalAlignment.Left);
            itemDetailsListView.Columns.Insert(3, "Units In Stock", 299, HorizontalAlignment.Left);
            itemDetailsListView.Columns.Insert(4, "Price Per Unit (R)", 300, HorizontalAlignment.Left);
        }

        private void AddInitialItemDetailsToItemDetailsListView()
        {
            stockItemsFromDatabase = new StockItemController().GetStockDetails(true);
            ListViewItem itemDetails;

            foreach (StockItem stockItem in stockItemsFromDatabase)
            {
                itemDetails = new ListViewItem();
                itemDetails.Text = stockItem.ProductCode;
                itemDetails.SubItems.Add(stockItem.Description);
                itemDetails.SubItems.Add(stockItem.ExpiryDate.ToString("d MMMM yyyy"));
                itemDetails.SubItems.Add(stockItem.UnitsInStock.ToString());
                itemDetails.SubItems.Add(stockItem.PricePerUnit.ToString());

                itemDetailsListView.Items.Add(itemDetails);
            }
        }

        private void SetupForm()
        {
            searchTextBox.Text = "";
            searchButton.Enabled = false;
            this.Refresh();
        }

        private void searchTextBox_TextChanged(object sender, EventArgs e)
        {
            if(searchTextBox.Text == "")
            {
                itemDetailsListView.Clear();
                SetupItemDetailsListView();
                AddInitialItemDetailsToItemDetailsListView();
                searchButton.Enabled = false;
            }
            else
            {
                searchButton.Enabled = true;
            }
        }

        private void searchButton_Click(object sender, EventArgs e)
        {
            string searchString = searchTextBox.Text;
            string[] descriptions = new string[itemDetailsListView.Items.Count];
            StockItem stockItem;
            ArrayList stockItems = new ArrayList();
            ArrayList newStockItems;
            ListViewItem newItemDetails;

            for (int i = 0; i < itemDetailsListView.Items.Count; i++)
            {
                stockItem = new StockItem();
                stockItem.ProductCode = itemDetailsListView.Items[i].SubItems[0].Text;
                stockItem.Description = itemDetailsListView.Items[i].SubItems[1].Text;
                stockItem.ExpiryDate = Convert.ToDateTime(itemDetailsListView.Items[i].SubItems[2].Text);
                stockItem.UnitsInStock = Convert.ToInt32(itemDetailsListView.Items[i].SubItems[3].Text);
                stockItem.PricePerUnit = Convert.ToDecimal(itemDetailsListView.Items[i].SubItems[4].Text);

                stockItems.Add(stockItem);
            }

            newStockItems = Search.FindStockItemMatches(searchString, stockItems);
            newStockItems.Sort();
            itemDetailsListView.Clear();
            SetupItemDetailsListView();

            foreach (StockItem newStockItem in newStockItems)
            {
                newItemDetails = new ListViewItem();
                newItemDetails.Text = newStockItem.ProductCode;
                newItemDetails.SubItems.Add(newStockItem.Description);
                newItemDetails.SubItems.Add(newStockItem.ExpiryDate.ToString("d MMMM yyyy"));
                newItemDetails.SubItems.Add(newStockItem.UnitsInStock.ToString());
                newItemDetails.SubItems.Add(newStockItem.PricePerUnit.ToString());

                itemDetailsListView.Items.Add(newItemDetails);
            }
        }
    }
}
