﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Order_Processing_System.Domain_Layer;
using Order_Processing_System.Application_Logic_Layer;
using Order_Processing_System.Database_Layer;

namespace Order_Processing_System.Presentation_Layer
{
    public partial class CreateCustomerForm : Form
    {
        private Customer customer;
        private CustomerController customerController;
        public delegate void ReturnToMainFormButtonClickEventHandler(Button button);
        public event ReturnToMainFormButtonClickEventHandler ReturnToMainFormButtonClick;
       
        public CreateCustomerForm()
        {
            InitializeComponent();
            customerController = new CustomerController();
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        { 
            base.OnFormClosing(e);
            if (e.CloseReason == CloseReason.WindowsShutDown) return;
            // Confirm that the user wishes to exit the application.
            switch (MessageBox.Show(this, "Are you sure you want to exit the application?", "Exit Application", MessageBoxButtons.YesNo))
            {
                case DialogResult.No:
                    e.Cancel = true;
                    break;

                default:
                    break;
            }
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            switch (MessageBox.Show("Are you sure you want to cancel creating the customer?", "Cancel Create Customer", MessageBoxButtons.YesNo))
            {
                case DialogResult.No:
                    break;

                case DialogResult.Yes:
                                
                    OnReturnToMainFormButtonClick((Button)sender);
                    this.Visible = false;
                    break;
            }
        }

        private void OnReturnToMainFormButtonClick(Button button)
        {
            if(ReturnToMainFormButtonClick != null)
            {
                ReturnToMainFormButtonClick(button);
                tb_ID.Text = "";
                tb_ID.BackColor = Color.White;
                tb_FirstName.Text = "";
                tb_FirstName.BackColor = Color.White;
                tb_LastName.Text = "";
                tb_LastName.BackColor = Color.White;
                tb_Email.Text = "";
                tb_Email.BackColor = Color.White;
                tb_Address.Text = "";
                tb_Address.BackColor = Color.White;
                tb_Contact.Text = "";
                tb_Contact.BackColor = Color.White;
                this.Refresh();
            }
        }
        
        private void createButton_Click(object sender, EventArgs e)
        {
            bool validInput = true;

            tb_ID.BackColor = Color.White;
            tb_FirstName.BackColor = Color.White;
            tb_LastName.BackColor = Color.White;
            tb_Email.BackColor = Color.White;
            tb_Address.BackColor = Color.White;
            tb_Contact.BackColor = Color.White;


            // Check that all fields are valid.

            string id = tb_ID.Text;
            if (!CustomerValidation.IDNumberValidation(id))
            {
                tb_ID.BackColor = Color.Red;
                tb_ID.Focus();

                validInput = false;
            }
            
            string firstName = tb_FirstName.Text;
            if (!CustomerValidation.NameValidation(firstName))
            {
                tb_FirstName.BackColor = Color.Red;
                tb_FirstName.Focus();

                validInput = false;
            }
            
            string lastName = tb_LastName.Text;
            if (!CustomerValidation.NameValidation(lastName))
            {
                tb_LastName.BackColor = Color.Red;
                tb_LastName.Focus();

                validInput = false;
            }
            
            string email = tb_Email.Text;
            if (!CustomerValidation.EmailValidation(email))
            {
                tb_Email.BackColor = Color.Red;
                tb_Email.Focus();

                validInput = false;
            }
            
            string address = tb_Address.Text;
            if (!CustomerValidation.AddressValidation(address))
            {
                tb_Address.BackColor = Color.Red;
                tb_Address.Focus();

                validInput = false;
            }
            
            string contact = tb_Contact.Text;
            if (!CustomerValidation.ContactNumberValidation(contact))
            {
                tb_Contact.BackColor = Color.Red;
                tb_Contact.Focus();

                validInput = false;
            }

            if(validInput)
            {
                customer = customerController.GetCustomerDetails(id);

                if(customer.IDNumber == null)
                {
                    customer.IDNumber = id;
                    customer.Name = firstName;
                    customer.Surname = lastName;
                    customer.EmailAddress = email;
                    customer.DeliveryAddress = address;
                    customer.ContactNumber = contact;
                    customer.Blacklisted = false;

                    switch (MessageBox.Show("Are you sure you want to create the customer?", "Create Customer", MessageBoxButtons.YesNo))
                    {
                        case DialogResult.No:
                            break;

                        case DialogResult.Yes:
                            if (customerController.AddCustomerDetails(customer) == true)
                            {
                                MessageBox.Show("Customer created successfully.", "Create Customer", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                OnReturnToMainFormButtonClick((Button)sender);
                                this.Visible = false;
                            }
                            break;
                    }
                }
                else
                {
                    tb_ID.BackColor = Color.Red;
                    MessageBox.Show("A customer with the ID number entered already exists.", "Invalid ID Number", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            else
            {
                MessageBox.Show("Input is not valid in the indicated field(s).", "Invalid Input", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}