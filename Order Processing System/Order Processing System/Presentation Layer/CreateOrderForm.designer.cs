﻿namespace Order_Processing_System.Presentation_Layer
{
    partial class CreateOrderForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CreateOrderForm));
            this.poppelLogoPictureBox = new System.Windows.Forms.PictureBox();
            this.createOrderLabel = new System.Windows.Forms.Label();
            this.customerDetailsLabel = new System.Windows.Forms.Label();
            this.idNumberLabel = new System.Windows.Forms.Label();
            this.nameLabel = new System.Windows.Forms.Label();
            this.surnameLabel = new System.Windows.Forms.Label();
            this.emailAddressLabel = new System.Windows.Forms.Label();
            this.deliveryAddressLabel = new System.Windows.Forms.Label();
            this.contactNumberLabel = new System.Windows.Forms.Label();
            this.idNumberTextBox = new System.Windows.Forms.TextBox();
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.surnameTextBox = new System.Windows.Forms.TextBox();
            this.emailAddressTextBox = new System.Windows.Forms.TextBox();
            this.deliveryAddressTextBox = new System.Windows.Forms.TextBox();
            this.contactNumberTextBox = new System.Windows.Forms.TextBox();
            this.submitButton = new System.Windows.Forms.Button();
            this.customerStatusLabel = new System.Windows.Forms.Label();
            this.customerStatusTextBox = new System.Windows.Forms.TextBox();
            this.itemDetailsLabel = new System.Windows.Forms.Label();
            this.itemDetailsListView = new System.Windows.Forms.ListView();
            this.searchTextBox = new System.Windows.Forms.TextBox();
            this.searchButton = new System.Windows.Forms.Button();
            this.quantityLabel = new System.Windows.Forms.Label();
            this.quantityNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.addItemButton = new System.Windows.Forms.Button();
            this.orderDetailsLabel = new System.Windows.Forms.Label();
            this.orderDetailsListView = new System.Windows.Forms.ListView();
            this.submitOrderButton = new System.Windows.Forms.Button();
            this.removeItemButton = new System.Windows.Forms.Button();
            this.totalLabel = new System.Windows.Forms.Label();
            this.totalTextBox = new System.Windows.Forms.TextBox();
            this.backButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.poppelLogoPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.quantityNumericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // poppelLogoPictureBox
            // 
            this.poppelLogoPictureBox.Image = global::Order_Processing_System.Properties.Resources.Poppel_Logo;
            this.poppelLogoPictureBox.Location = new System.Drawing.Point(629, 12);
            this.poppelLogoPictureBox.Name = "poppelLogoPictureBox";
            this.poppelLogoPictureBox.Size = new System.Drawing.Size(311, 180);
            this.poppelLogoPictureBox.TabIndex = 1;
            this.poppelLogoPictureBox.TabStop = false;
            // 
            // createOrderLabel
            // 
            this.createOrderLabel.AutoSize = true;
            this.createOrderLabel.Font = new System.Drawing.Font("Arial Narrow", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.createOrderLabel.Location = new System.Drawing.Point(730, 209);
            this.createOrderLabel.Name = "createOrderLabel";
            this.createOrderLabel.Size = new System.Drawing.Size(109, 24);
            this.createOrderLabel.TabIndex = 2;
            this.createOrderLabel.Text = "Create Order";
            // 
            // customerDetailsLabel
            // 
            this.customerDetailsLabel.AutoSize = true;
            this.customerDetailsLabel.Font = new System.Drawing.Font("Arial Narrow", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.customerDetailsLabel.Location = new System.Drawing.Point(12, 264);
            this.customerDetailsLabel.Name = "customerDetailsLabel";
            this.customerDetailsLabel.Size = new System.Drawing.Size(139, 23);
            this.customerDetailsLabel.TabIndex = 3;
            this.customerDetailsLabel.Text = "Customer Details";
            // 
            // idNumberLabel
            // 
            this.idNumberLabel.AutoSize = true;
            this.idNumberLabel.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.idNumberLabel.Location = new System.Drawing.Point(12, 324);
            this.idNumberLabel.Name = "idNumberLabel";
            this.idNumberLabel.Size = new System.Drawing.Size(82, 22);
            this.idNumberLabel.TabIndex = 4;
            this.idNumberLabel.Text = "ID Number";
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nameLabel.Location = new System.Drawing.Point(12, 366);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(48, 22);
            this.nameLabel.TabIndex = 5;
            this.nameLabel.Text = "Name";
            // 
            // surnameLabel
            // 
            this.surnameLabel.AutoSize = true;
            this.surnameLabel.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.surnameLabel.Location = new System.Drawing.Point(12, 408);
            this.surnameLabel.Name = "surnameLabel";
            this.surnameLabel.Size = new System.Drawing.Size(71, 22);
            this.surnameLabel.TabIndex = 7;
            this.surnameLabel.Text = "Surname";
            // 
            // emailAddressLabel
            // 
            this.emailAddressLabel.AutoSize = true;
            this.emailAddressLabel.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.emailAddressLabel.Location = new System.Drawing.Point(12, 450);
            this.emailAddressLabel.Name = "emailAddressLabel";
            this.emailAddressLabel.Size = new System.Drawing.Size(109, 22);
            this.emailAddressLabel.TabIndex = 9;
            this.emailAddressLabel.Text = "Email Address";
            // 
            // deliveryAddressLabel
            // 
            this.deliveryAddressLabel.AutoSize = true;
            this.deliveryAddressLabel.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deliveryAddressLabel.Location = new System.Drawing.Point(12, 492);
            this.deliveryAddressLabel.Name = "deliveryAddressLabel";
            this.deliveryAddressLabel.Size = new System.Drawing.Size(128, 22);
            this.deliveryAddressLabel.TabIndex = 11;
            this.deliveryAddressLabel.Text = "Delivery Address";
            // 
            // contactNumberLabel
            // 
            this.contactNumberLabel.AutoSize = true;
            this.contactNumberLabel.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.contactNumberLabel.Location = new System.Drawing.Point(12, 534);
            this.contactNumberLabel.Name = "contactNumberLabel";
            this.contactNumberLabel.Size = new System.Drawing.Size(122, 22);
            this.contactNumberLabel.TabIndex = 13;
            this.contactNumberLabel.Text = "Contact Number";
            // 
            // idNumberTextBox
            // 
            this.idNumberTextBox.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.idNumberTextBox.Location = new System.Drawing.Point(146, 324);
            this.idNumberTextBox.Name = "idNumberTextBox";
            this.idNumberTextBox.Size = new System.Drawing.Size(557, 27);
            this.idNumberTextBox.TabIndex = 14;
            this.idNumberTextBox.TextChanged += new System.EventHandler(this.idNumberTextBox_TextChanged);
            // 
            // nameTextBox
            // 
            this.nameTextBox.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nameTextBox.Location = new System.Drawing.Point(146, 366);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.ReadOnly = true;
            this.nameTextBox.Size = new System.Drawing.Size(557, 27);
            this.nameTextBox.TabIndex = 15;
            // 
            // surnameTextBox
            // 
            this.surnameTextBox.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.surnameTextBox.Location = new System.Drawing.Point(146, 408);
            this.surnameTextBox.Name = "surnameTextBox";
            this.surnameTextBox.ReadOnly = true;
            this.surnameTextBox.Size = new System.Drawing.Size(557, 27);
            this.surnameTextBox.TabIndex = 16;
            // 
            // emailAddressTextBox
            // 
            this.emailAddressTextBox.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.emailAddressTextBox.Location = new System.Drawing.Point(146, 450);
            this.emailAddressTextBox.Name = "emailAddressTextBox";
            this.emailAddressTextBox.ReadOnly = true;
            this.emailAddressTextBox.Size = new System.Drawing.Size(557, 27);
            this.emailAddressTextBox.TabIndex = 17;
            // 
            // deliveryAddressTextBox
            // 
            this.deliveryAddressTextBox.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deliveryAddressTextBox.Location = new System.Drawing.Point(146, 492);
            this.deliveryAddressTextBox.Name = "deliveryAddressTextBox";
            this.deliveryAddressTextBox.ReadOnly = true;
            this.deliveryAddressTextBox.Size = new System.Drawing.Size(557, 27);
            this.deliveryAddressTextBox.TabIndex = 18;
            // 
            // contactNumberTextBox
            // 
            this.contactNumberTextBox.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.contactNumberTextBox.Location = new System.Drawing.Point(146, 534);
            this.contactNumberTextBox.Name = "contactNumberTextBox";
            this.contactNumberTextBox.ReadOnly = true;
            this.contactNumberTextBox.Size = new System.Drawing.Size(557, 27);
            this.contactNumberTextBox.TabIndex = 19;
            // 
            // submitButton
            // 
            this.submitButton.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.submitButton.Location = new System.Drawing.Point(709, 324);
            this.submitButton.Name = "submitButton";
            this.submitButton.Size = new System.Drawing.Size(84, 27);
            this.submitButton.TabIndex = 20;
            this.submitButton.Text = "Submit";
            this.submitButton.UseVisualStyleBackColor = true;
            this.submitButton.Click += new System.EventHandler(this.submitButton_Click);
            // 
            // customerStatusLabel
            // 
            this.customerStatusLabel.AutoSize = true;
            this.customerStatusLabel.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.customerStatusLabel.ForeColor = System.Drawing.Color.Red;
            this.customerStatusLabel.Location = new System.Drawing.Point(820, 327);
            this.customerStatusLabel.Name = "customerStatusLabel";
            this.customerStatusLabel.Size = new System.Drawing.Size(151, 22);
            this.customerStatusLabel.TabIndex = 21;
            this.customerStatusLabel.Text = "CUSTOMER STATUS";
            // 
            // customerStatusTextBox
            // 
            this.customerStatusTextBox.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.customerStatusTextBox.Location = new System.Drawing.Point(977, 327);
            this.customerStatusTextBox.Name = "customerStatusTextBox";
            this.customerStatusTextBox.ReadOnly = true;
            this.customerStatusTextBox.Size = new System.Drawing.Size(557, 27);
            this.customerStatusTextBox.TabIndex = 22;
            // 
            // itemDetailsLabel
            // 
            this.itemDetailsLabel.AutoSize = true;
            this.itemDetailsLabel.Font = new System.Drawing.Font("Arial Narrow", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.itemDetailsLabel.Location = new System.Drawing.Point(12, 594);
            this.itemDetailsLabel.Name = "itemDetailsLabel";
            this.itemDetailsLabel.Size = new System.Drawing.Size(97, 23);
            this.itemDetailsLabel.TabIndex = 23;
            this.itemDetailsLabel.Text = "Item Details";
            // 
            // itemDetailsListView
            // 
            this.itemDetailsListView.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.itemDetailsListView.FullRowSelect = true;
            this.itemDetailsListView.GridLines = true;
            this.itemDetailsListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.itemDetailsListView.HideSelection = false;
            this.itemDetailsListView.Location = new System.Drawing.Point(12, 654);
            this.itemDetailsListView.MultiSelect = false;
            this.itemDetailsListView.Name = "itemDetailsListView";
            this.itemDetailsListView.Size = new System.Drawing.Size(1518, 162);
            this.itemDetailsListView.TabIndex = 24;
            this.itemDetailsListView.UseCompatibleStateImageBehavior = false;
            this.itemDetailsListView.SelectedIndexChanged += new System.EventHandler(this.itemDetailsListView_SelectedIndexChanged);
            // 
            // searchTextBox
            // 
            this.searchTextBox.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchTextBox.Location = new System.Drawing.Point(1280, 594);
            this.searchTextBox.Name = "searchTextBox";
            this.searchTextBox.Size = new System.Drawing.Size(160, 27);
            this.searchTextBox.TabIndex = 26;
            this.searchTextBox.TextChanged += new System.EventHandler(this.searchTextBox_TextChanged);
            // 
            // searchButton
            // 
            this.searchButton.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchButton.Location = new System.Drawing.Point(1446, 594);
            this.searchButton.Name = "searchButton";
            this.searchButton.Size = new System.Drawing.Size(84, 27);
            this.searchButton.TabIndex = 27;
            this.searchButton.Text = "Search";
            this.searchButton.UseVisualStyleBackColor = true;
            this.searchButton.Click += new System.EventHandler(this.searchButton_Click);
            // 
            // quantityLabel
            // 
            this.quantityLabel.AutoSize = true;
            this.quantityLabel.Font = new System.Drawing.Font("Arial Narrow", 10.8F, System.Drawing.FontStyle.Bold);
            this.quantityLabel.Location = new System.Drawing.Point(12, 877);
            this.quantityLabel.Name = "quantityLabel";
            this.quantityLabel.Size = new System.Drawing.Size(74, 23);
            this.quantityLabel.TabIndex = 28;
            this.quantityLabel.Text = "Quantity";
            // 
            // quantityNumericUpDown
            // 
            this.quantityNumericUpDown.Font = new System.Drawing.Font("Arial Narrow", 10.2F);
            this.quantityNumericUpDown.Location = new System.Drawing.Point(92, 876);
            this.quantityNumericUpDown.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.quantityNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.quantityNumericUpDown.Name = "quantityNumericUpDown";
            this.quantityNumericUpDown.Size = new System.Drawing.Size(60, 27);
            this.quantityNumericUpDown.TabIndex = 29;
            this.quantityNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // addItemButton
            // 
            this.addItemButton.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addItemButton.Location = new System.Drawing.Point(1356, 866);
            this.addItemButton.Name = "addItemButton";
            this.addItemButton.Size = new System.Drawing.Size(174, 46);
            this.addItemButton.TabIndex = 30;
            this.addItemButton.Text = "Add Item";
            this.addItemButton.UseVisualStyleBackColor = true;
            this.addItemButton.Click += new System.EventHandler(this.addItemButton_Click);
            // 
            // orderDetailsLabel
            // 
            this.orderDetailsLabel.AutoSize = true;
            this.orderDetailsLabel.Font = new System.Drawing.Font("Arial Narrow", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.orderDetailsLabel.Location = new System.Drawing.Point(12, 937);
            this.orderDetailsLabel.Name = "orderDetailsLabel";
            this.orderDetailsLabel.Size = new System.Drawing.Size(108, 23);
            this.orderDetailsLabel.TabIndex = 31;
            this.orderDetailsLabel.Text = "Order Details";
            // 
            // orderDetailsListView
            // 
            this.orderDetailsListView.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.orderDetailsListView.FullRowSelect = true;
            this.orderDetailsListView.GridLines = true;
            this.orderDetailsListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.orderDetailsListView.HideSelection = false;
            this.orderDetailsListView.Location = new System.Drawing.Point(12, 997);
            this.orderDetailsListView.MultiSelect = false;
            this.orderDetailsListView.Name = "orderDetailsListView";
            this.orderDetailsListView.Size = new System.Drawing.Size(1518, 162);
            this.orderDetailsListView.TabIndex = 32;
            this.orderDetailsListView.UseCompatibleStateImageBehavior = false;
            this.orderDetailsListView.SelectedIndexChanged += new System.EventHandler(this.orderDetailsListView_SelectedIndexChanged);
            // 
            // submitOrderButton
            // 
            this.submitOrderButton.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.submitOrderButton.Location = new System.Drawing.Point(1356, 1365);
            this.submitOrderButton.Name = "submitOrderButton";
            this.submitOrderButton.Size = new System.Drawing.Size(174, 46);
            this.submitOrderButton.TabIndex = 34;
            this.submitOrderButton.Text = "Submit Order";
            this.submitOrderButton.UseVisualStyleBackColor = true;
            this.submitOrderButton.Click += new System.EventHandler(this.submitOrderButton_Click);
            // 
            // removeItemButton
            // 
            this.removeItemButton.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.removeItemButton.Location = new System.Drawing.Point(12, 1219);
            this.removeItemButton.Name = "removeItemButton";
            this.removeItemButton.Size = new System.Drawing.Size(174, 46);
            this.removeItemButton.TabIndex = 35;
            this.removeItemButton.Text = "Remove Item";
            this.removeItemButton.UseVisualStyleBackColor = true;
            this.removeItemButton.Click += new System.EventHandler(this.removeItemButton_Click);
            // 
            // totalLabel
            // 
            this.totalLabel.AutoSize = true;
            this.totalLabel.Font = new System.Drawing.Font("Arial Narrow", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalLabel.Location = new System.Drawing.Point(1356, 1230);
            this.totalLabel.Name = "totalLabel";
            this.totalLabel.Size = new System.Drawing.Size(68, 23);
            this.totalLabel.TabIndex = 36;
            this.totalLabel.Text = "Total: R";
            // 
            // totalTextBox
            // 
            this.totalTextBox.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalTextBox.Location = new System.Drawing.Point(1430, 1230);
            this.totalTextBox.Name = "totalTextBox";
            this.totalTextBox.ReadOnly = true;
            this.totalTextBox.Size = new System.Drawing.Size(100, 27);
            this.totalTextBox.TabIndex = 37;
            // 
            // backButton
            // 
            this.backButton.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.backButton.Location = new System.Drawing.Point(12, 1365);
            this.backButton.Name = "backButton";
            this.backButton.Size = new System.Drawing.Size(174, 46);
            this.backButton.TabIndex = 38;
            this.backButton.Text = "Back";
            this.backButton.UseVisualStyleBackColor = true;
            this.backButton.Click += new System.EventHandler(this.backButton_Click);
            // 
            // CreateOrderForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoScrollMargin = new System.Drawing.Size(0, 12);
            this.ClientSize = new System.Drawing.Size(1546, 851);
            this.Controls.Add(this.backButton);
            this.Controls.Add(this.totalTextBox);
            this.Controls.Add(this.totalLabel);
            this.Controls.Add(this.removeItemButton);
            this.Controls.Add(this.submitOrderButton);
            this.Controls.Add(this.orderDetailsListView);
            this.Controls.Add(this.orderDetailsLabel);
            this.Controls.Add(this.addItemButton);
            this.Controls.Add(this.quantityNumericUpDown);
            this.Controls.Add(this.quantityLabel);
            this.Controls.Add(this.searchButton);
            this.Controls.Add(this.searchTextBox);
            this.Controls.Add(this.itemDetailsListView);
            this.Controls.Add(this.itemDetailsLabel);
            this.Controls.Add(this.customerStatusTextBox);
            this.Controls.Add(this.customerStatusLabel);
            this.Controls.Add(this.submitButton);
            this.Controls.Add(this.contactNumberTextBox);
            this.Controls.Add(this.deliveryAddressTextBox);
            this.Controls.Add(this.emailAddressTextBox);
            this.Controls.Add(this.surnameTextBox);
            this.Controls.Add(this.nameTextBox);
            this.Controls.Add(this.idNumberTextBox);
            this.Controls.Add(this.contactNumberLabel);
            this.Controls.Add(this.deliveryAddressLabel);
            this.Controls.Add(this.emailAddressLabel);
            this.Controls.Add(this.surnameLabel);
            this.Controls.Add(this.nameLabel);
            this.Controls.Add(this.idNumberLabel);
            this.Controls.Add(this.customerDetailsLabel);
            this.Controls.Add(this.createOrderLabel);
            this.Controls.Add(this.poppelLogoPictureBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "CreateOrderForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Create Order Form";
            ((System.ComponentModel.ISupportInitialize)(this.poppelLogoPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.quantityNumericUpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox poppelLogoPictureBox;
        private System.Windows.Forms.Label createOrderLabel;
        private System.Windows.Forms.Label customerDetailsLabel;
        private System.Windows.Forms.Label idNumberLabel;
        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.Label surnameLabel;
        private System.Windows.Forms.Label emailAddressLabel;
        private System.Windows.Forms.Label deliveryAddressLabel;
        private System.Windows.Forms.Label contactNumberLabel;
        private System.Windows.Forms.TextBox idNumberTextBox;
        private System.Windows.Forms.TextBox nameTextBox;
        private System.Windows.Forms.TextBox surnameTextBox;
        private System.Windows.Forms.TextBox emailAddressTextBox;
        private System.Windows.Forms.TextBox deliveryAddressTextBox;
        private System.Windows.Forms.TextBox contactNumberTextBox;
        private System.Windows.Forms.Button submitButton;
        private System.Windows.Forms.Label customerStatusLabel;
        private System.Windows.Forms.TextBox customerStatusTextBox;
        private System.Windows.Forms.Label itemDetailsLabel;
        private System.Windows.Forms.ListView itemDetailsListView;
        private System.Windows.Forms.TextBox searchTextBox;
        private System.Windows.Forms.Button searchButton;
        private System.Windows.Forms.Label quantityLabel;
        private System.Windows.Forms.NumericUpDown quantityNumericUpDown;
        private System.Windows.Forms.Button addItemButton;
        private System.Windows.Forms.Label orderDetailsLabel;
        private System.Windows.Forms.ListView orderDetailsListView;
        private System.Windows.Forms.Button submitOrderButton;
        private System.Windows.Forms.Button removeItemButton;
        private System.Windows.Forms.Label totalLabel;
        private System.Windows.Forms.TextBox totalTextBox;
        private System.Windows.Forms.Button backButton;
    }
}