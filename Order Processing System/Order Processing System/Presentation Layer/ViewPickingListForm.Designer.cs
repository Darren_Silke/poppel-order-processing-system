﻿namespace Order_Processing_System.Presentation_Layer
{
    partial class ViewPickingListForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ViewPickingListForm));
            this.poppelLogoPictureBox = new System.Windows.Forms.PictureBox();
            this.viewPickingListLabel = new System.Windows.Forms.Label();
            this.customerOrdersLabel = new System.Windows.Forms.Label();
            this.searchTextBox = new System.Windows.Forms.TextBox();
            this.searchButton = new System.Windows.Forms.Button();
            this.customerOrdersListView = new System.Windows.Forms.ListView();
            this.viewDetailsButton = new System.Windows.Forms.Button();
            this.customerDetailsLabel = new System.Windows.Forms.Label();
            this.idNumberLabel = new System.Windows.Forms.Label();
            this.nameLabel = new System.Windows.Forms.Label();
            this.surnameLabel = new System.Windows.Forms.Label();
            this.emailAddressLabel = new System.Windows.Forms.Label();
            this.deliveryAddressLabel = new System.Windows.Forms.Label();
            this.contactNumberLabel = new System.Windows.Forms.Label();
            this.idNumberTextBox = new System.Windows.Forms.TextBox();
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.surnameTextBox = new System.Windows.Forms.TextBox();
            this.emailAddressTextBox = new System.Windows.Forms.TextBox();
            this.deliveryAddressTextBox = new System.Windows.Forms.TextBox();
            this.contactNumberTextBox = new System.Windows.Forms.TextBox();
            this.orderDetailsLabel = new System.Windows.Forms.Label();
            this.orderDetailsListView = new System.Windows.Forms.ListView();
            this.totalLabel = new System.Windows.Forms.Label();
            this.totalTextBox = new System.Windows.Forms.TextBox();
            this.backButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.poppelLogoPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // poppelLogoPictureBox
            // 
            this.poppelLogoPictureBox.Image = global::Order_Processing_System.Properties.Resources.Poppel_Logo;
            this.poppelLogoPictureBox.Location = new System.Drawing.Point(629, 12);
            this.poppelLogoPictureBox.Name = "poppelLogoPictureBox";
            this.poppelLogoPictureBox.Size = new System.Drawing.Size(311, 180);
            this.poppelLogoPictureBox.TabIndex = 3;
            this.poppelLogoPictureBox.TabStop = false;
            // 
            // viewPickingListLabel
            // 
            this.viewPickingListLabel.AutoSize = true;
            this.viewPickingListLabel.Font = new System.Drawing.Font("Arial Narrow", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.viewPickingListLabel.Location = new System.Drawing.Point(713, 209);
            this.viewPickingListLabel.Name = "viewPickingListLabel";
            this.viewPickingListLabel.Size = new System.Drawing.Size(142, 24);
            this.viewPickingListLabel.TabIndex = 4;
            this.viewPickingListLabel.Text = "View Picking List";
            // 
            // customerOrdersLabel
            // 
            this.customerOrdersLabel.AutoSize = true;
            this.customerOrdersLabel.Font = new System.Drawing.Font("Arial Narrow", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.customerOrdersLabel.Location = new System.Drawing.Point(12, 264);
            this.customerOrdersLabel.Name = "customerOrdersLabel";
            this.customerOrdersLabel.Size = new System.Drawing.Size(140, 23);
            this.customerOrdersLabel.TabIndex = 25;
            this.customerOrdersLabel.Text = "Customer Orders";
            // 
            // searchTextBox
            // 
            this.searchTextBox.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchTextBox.Location = new System.Drawing.Point(1280, 264);
            this.searchTextBox.Name = "searchTextBox";
            this.searchTextBox.Size = new System.Drawing.Size(160, 27);
            this.searchTextBox.TabIndex = 28;
            this.searchTextBox.TextChanged += new System.EventHandler(this.searchTextBox_TextChanged);
            // 
            // searchButton
            // 
            this.searchButton.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchButton.Location = new System.Drawing.Point(1446, 264);
            this.searchButton.Name = "searchButton";
            this.searchButton.Size = new System.Drawing.Size(84, 27);
            this.searchButton.TabIndex = 29;
            this.searchButton.Text = "Search";
            this.searchButton.UseVisualStyleBackColor = true;
            this.searchButton.Click += new System.EventHandler(this.searchButton_Click);
            // 
            // customerOrdersListView
            // 
            this.customerOrdersListView.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.customerOrdersListView.FullRowSelect = true;
            this.customerOrdersListView.GridLines = true;
            this.customerOrdersListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.customerOrdersListView.HideSelection = false;
            this.customerOrdersListView.Location = new System.Drawing.Point(12, 324);
            this.customerOrdersListView.MultiSelect = false;
            this.customerOrdersListView.Name = "customerOrdersListView";
            this.customerOrdersListView.Size = new System.Drawing.Size(1518, 162);
            this.customerOrdersListView.TabIndex = 30;
            this.customerOrdersListView.UseCompatibleStateImageBehavior = false;
            this.customerOrdersListView.SelectedIndexChanged += new System.EventHandler(this.customerOrdersListView_SelectedIndexChanged);
            // 
            // viewDetailsButton
            // 
            this.viewDetailsButton.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.viewDetailsButton.Location = new System.Drawing.Point(697, 546);
            this.viewDetailsButton.Name = "viewDetailsButton";
            this.viewDetailsButton.Size = new System.Drawing.Size(174, 46);
            this.viewDetailsButton.TabIndex = 40;
            this.viewDetailsButton.Text = "View Details";
            this.viewDetailsButton.UseVisualStyleBackColor = true;
            this.viewDetailsButton.Click += new System.EventHandler(this.viewDetailsButton_Click);
            // 
            // customerDetailsLabel
            // 
            this.customerDetailsLabel.AutoSize = true;
            this.customerDetailsLabel.Font = new System.Drawing.Font("Arial Narrow", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.customerDetailsLabel.Location = new System.Drawing.Point(12, 652);
            this.customerDetailsLabel.Name = "customerDetailsLabel";
            this.customerDetailsLabel.Size = new System.Drawing.Size(139, 23);
            this.customerDetailsLabel.TabIndex = 41;
            this.customerDetailsLabel.Text = "Customer Details";
            // 
            // idNumberLabel
            // 
            this.idNumberLabel.AutoSize = true;
            this.idNumberLabel.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.idNumberLabel.Location = new System.Drawing.Point(12, 712);
            this.idNumberLabel.Name = "idNumberLabel";
            this.idNumberLabel.Size = new System.Drawing.Size(82, 22);
            this.idNumberLabel.TabIndex = 42;
            this.idNumberLabel.Text = "ID Number";
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nameLabel.Location = new System.Drawing.Point(12, 754);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(48, 22);
            this.nameLabel.TabIndex = 43;
            this.nameLabel.Text = "Name";
            // 
            // surnameLabel
            // 
            this.surnameLabel.AutoSize = true;
            this.surnameLabel.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.surnameLabel.Location = new System.Drawing.Point(12, 796);
            this.surnameLabel.Name = "surnameLabel";
            this.surnameLabel.Size = new System.Drawing.Size(71, 22);
            this.surnameLabel.TabIndex = 44;
            this.surnameLabel.Text = "Surname";
            // 
            // emailAddressLabel
            // 
            this.emailAddressLabel.AutoSize = true;
            this.emailAddressLabel.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.emailAddressLabel.Location = new System.Drawing.Point(12, 838);
            this.emailAddressLabel.Name = "emailAddressLabel";
            this.emailAddressLabel.Size = new System.Drawing.Size(109, 22);
            this.emailAddressLabel.TabIndex = 45;
            this.emailAddressLabel.Text = "Email Address";
            // 
            // deliveryAddressLabel
            // 
            this.deliveryAddressLabel.AutoSize = true;
            this.deliveryAddressLabel.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deliveryAddressLabel.Location = new System.Drawing.Point(12, 880);
            this.deliveryAddressLabel.Name = "deliveryAddressLabel";
            this.deliveryAddressLabel.Size = new System.Drawing.Size(128, 22);
            this.deliveryAddressLabel.TabIndex = 46;
            this.deliveryAddressLabel.Text = "Delivery Address";
            // 
            // contactNumberLabel
            // 
            this.contactNumberLabel.AutoSize = true;
            this.contactNumberLabel.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.contactNumberLabel.Location = new System.Drawing.Point(12, 922);
            this.contactNumberLabel.Name = "contactNumberLabel";
            this.contactNumberLabel.Size = new System.Drawing.Size(122, 22);
            this.contactNumberLabel.TabIndex = 47;
            this.contactNumberLabel.Text = "Contact Number";
            // 
            // idNumberTextBox
            // 
            this.idNumberTextBox.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.idNumberTextBox.Location = new System.Drawing.Point(146, 707);
            this.idNumberTextBox.Name = "idNumberTextBox";
            this.idNumberTextBox.ReadOnly = true;
            this.idNumberTextBox.Size = new System.Drawing.Size(557, 27);
            this.idNumberTextBox.TabIndex = 48;
            // 
            // nameTextBox
            // 
            this.nameTextBox.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nameTextBox.Location = new System.Drawing.Point(146, 749);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.ReadOnly = true;
            this.nameTextBox.Size = new System.Drawing.Size(557, 27);
            this.nameTextBox.TabIndex = 49;
            // 
            // surnameTextBox
            // 
            this.surnameTextBox.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.surnameTextBox.Location = new System.Drawing.Point(146, 791);
            this.surnameTextBox.Name = "surnameTextBox";
            this.surnameTextBox.ReadOnly = true;
            this.surnameTextBox.Size = new System.Drawing.Size(557, 27);
            this.surnameTextBox.TabIndex = 50;
            // 
            // emailAddressTextBox
            // 
            this.emailAddressTextBox.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.emailAddressTextBox.Location = new System.Drawing.Point(146, 833);
            this.emailAddressTextBox.Name = "emailAddressTextBox";
            this.emailAddressTextBox.ReadOnly = true;
            this.emailAddressTextBox.Size = new System.Drawing.Size(557, 27);
            this.emailAddressTextBox.TabIndex = 51;
            // 
            // deliveryAddressTextBox
            // 
            this.deliveryAddressTextBox.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deliveryAddressTextBox.Location = new System.Drawing.Point(146, 875);
            this.deliveryAddressTextBox.Name = "deliveryAddressTextBox";
            this.deliveryAddressTextBox.ReadOnly = true;
            this.deliveryAddressTextBox.Size = new System.Drawing.Size(557, 27);
            this.deliveryAddressTextBox.TabIndex = 52;
            // 
            // contactNumberTextBox
            // 
            this.contactNumberTextBox.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.contactNumberTextBox.Location = new System.Drawing.Point(146, 917);
            this.contactNumberTextBox.Name = "contactNumberTextBox";
            this.contactNumberTextBox.ReadOnly = true;
            this.contactNumberTextBox.Size = new System.Drawing.Size(557, 27);
            this.contactNumberTextBox.TabIndex = 53;
            // 
            // orderDetailsLabel
            // 
            this.orderDetailsLabel.AutoSize = true;
            this.orderDetailsLabel.Font = new System.Drawing.Font("Arial Narrow", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.orderDetailsLabel.Location = new System.Drawing.Point(12, 982);
            this.orderDetailsLabel.Name = "orderDetailsLabel";
            this.orderDetailsLabel.Size = new System.Drawing.Size(108, 23);
            this.orderDetailsLabel.TabIndex = 54;
            this.orderDetailsLabel.Text = "Order Details";
            // 
            // orderDetailsListView
            // 
            this.orderDetailsListView.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.orderDetailsListView.FullRowSelect = true;
            this.orderDetailsListView.GridLines = true;
            this.orderDetailsListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.orderDetailsListView.HideSelection = false;
            this.orderDetailsListView.Location = new System.Drawing.Point(12, 1042);
            this.orderDetailsListView.MultiSelect = false;
            this.orderDetailsListView.Name = "orderDetailsListView";
            this.orderDetailsListView.Size = new System.Drawing.Size(1518, 162);
            this.orderDetailsListView.TabIndex = 55;
            this.orderDetailsListView.UseCompatibleStateImageBehavior = false;
            // 
            // totalLabel
            // 
            this.totalLabel.AutoSize = true;
            this.totalLabel.Font = new System.Drawing.Font("Arial Narrow", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalLabel.Location = new System.Drawing.Point(1356, 1264);
            this.totalLabel.Name = "totalLabel";
            this.totalLabel.Size = new System.Drawing.Size(68, 23);
            this.totalLabel.TabIndex = 58;
            this.totalLabel.Text = "Total: R";
            // 
            // totalTextBox
            // 
            this.totalTextBox.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalTextBox.Location = new System.Drawing.Point(1430, 1264);
            this.totalTextBox.Name = "totalTextBox";
            this.totalTextBox.ReadOnly = true;
            this.totalTextBox.Size = new System.Drawing.Size(100, 27);
            this.totalTextBox.TabIndex = 59;
            // 
            // backButton
            // 
            this.backButton.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.backButton.Location = new System.Drawing.Point(12, 1364);
            this.backButton.Name = "backButton";
            this.backButton.Size = new System.Drawing.Size(174, 46);
            this.backButton.TabIndex = 60;
            this.backButton.Text = "Back";
            this.backButton.UseVisualStyleBackColor = true;
            this.backButton.Click += new System.EventHandler(this.backButton_Click);
            // 
            // ViewPickingListForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoScrollMargin = new System.Drawing.Size(0, 12);
            this.ClientSize = new System.Drawing.Size(1546, 851);
            this.Controls.Add(this.backButton);
            this.Controls.Add(this.totalTextBox);
            this.Controls.Add(this.totalLabel);
            this.Controls.Add(this.orderDetailsListView);
            this.Controls.Add(this.orderDetailsLabel);
            this.Controls.Add(this.contactNumberTextBox);
            this.Controls.Add(this.deliveryAddressTextBox);
            this.Controls.Add(this.emailAddressTextBox);
            this.Controls.Add(this.surnameTextBox);
            this.Controls.Add(this.nameTextBox);
            this.Controls.Add(this.idNumberTextBox);
            this.Controls.Add(this.contactNumberLabel);
            this.Controls.Add(this.deliveryAddressLabel);
            this.Controls.Add(this.emailAddressLabel);
            this.Controls.Add(this.surnameLabel);
            this.Controls.Add(this.nameLabel);
            this.Controls.Add(this.idNumberLabel);
            this.Controls.Add(this.customerDetailsLabel);
            this.Controls.Add(this.viewDetailsButton);
            this.Controls.Add(this.customerOrdersListView);
            this.Controls.Add(this.searchButton);
            this.Controls.Add(this.searchTextBox);
            this.Controls.Add(this.customerOrdersLabel);
            this.Controls.Add(this.viewPickingListLabel);
            this.Controls.Add(this.poppelLogoPictureBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "ViewPickingListForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "View Picking List Form";
            this.VisibleChanged += new System.EventHandler(this.ViewPickingListForm_VisibleChanged);
            ((System.ComponentModel.ISupportInitialize)(this.poppelLogoPictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox poppelLogoPictureBox;
        private System.Windows.Forms.Label viewPickingListLabel;
        private System.Windows.Forms.Label customerOrdersLabel;
        private System.Windows.Forms.TextBox searchTextBox;
        private System.Windows.Forms.Button searchButton;
        private System.Windows.Forms.ListView customerOrdersListView;
        private System.Windows.Forms.Button viewDetailsButton;
        private System.Windows.Forms.Label customerDetailsLabel;
        private System.Windows.Forms.Label idNumberLabel;
        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.Label surnameLabel;
        private System.Windows.Forms.Label emailAddressLabel;
        private System.Windows.Forms.Label deliveryAddressLabel;
        private System.Windows.Forms.Label contactNumberLabel;
        private System.Windows.Forms.TextBox idNumberTextBox;
        private System.Windows.Forms.TextBox nameTextBox;
        private System.Windows.Forms.TextBox surnameTextBox;
        private System.Windows.Forms.TextBox emailAddressTextBox;
        private System.Windows.Forms.TextBox deliveryAddressTextBox;
        private System.Windows.Forms.TextBox contactNumberTextBox;
        private System.Windows.Forms.Label orderDetailsLabel;
        private System.Windows.Forms.ListView orderDetailsListView;
        private System.Windows.Forms.Label totalLabel;
        private System.Windows.Forms.TextBox totalTextBox;
        private System.Windows.Forms.Button backButton;
    }
}