﻿namespace Order_Processing_System.Presentation_Layer
{
    partial class CreateCustomerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CreateCustomerForm));
            this.img_Logo = new System.Windows.Forms.PictureBox();
            this.lb_Customer = new System.Windows.Forms.Label();
            this.lb_ID = new System.Windows.Forms.Label();
            this.tb_ID = new System.Windows.Forms.TextBox();
            this.lb_FirstName = new System.Windows.Forms.Label();
            this.tb_FirstName = new System.Windows.Forms.TextBox();
            this.lb_LastName = new System.Windows.Forms.Label();
            this.tb_LastName = new System.Windows.Forms.TextBox();
            this.lb_Email = new System.Windows.Forms.Label();
            this.tb_Email = new System.Windows.Forms.TextBox();
            this.lb_Address = new System.Windows.Forms.Label();
            this.tb_Address = new System.Windows.Forms.TextBox();
            this.lb_Contact = new System.Windows.Forms.Label();
            this.tb_Contact = new System.Windows.Forms.TextBox();
            this.btn_Create = new System.Windows.Forms.Button();
            this.btn_Back = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.img_Logo)).BeginInit();
            this.SuspendLayout();
            // 
            // img_Logo
            // 
            this.img_Logo.Image = global::Order_Processing_System.Properties.Resources.Poppel_Logo;
            this.img_Logo.Location = new System.Drawing.Point(213, 12);
            this.img_Logo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.img_Logo.Name = "img_Logo";
            this.img_Logo.Size = new System.Drawing.Size(311, 180);
            this.img_Logo.TabIndex = 0;
            this.img_Logo.TabStop = false;
            // 
            // lb_Customer
            // 
            this.lb_Customer.AutoSize = true;
            this.lb_Customer.Font = new System.Drawing.Font("Arial Narrow", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_Customer.Location = new System.Drawing.Point(299, 209);
            this.lb_Customer.Name = "lb_Customer";
            this.lb_Customer.Size = new System.Drawing.Size(140, 24);
            this.lb_Customer.TabIndex = 1;
            this.lb_Customer.Text = "Create Customer";
            // 
            // lb_ID
            // 
            this.lb_ID.AutoSize = true;
            this.lb_ID.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_ID.Location = new System.Drawing.Point(12, 251);
            this.lb_ID.Name = "lb_ID";
            this.lb_ID.Size = new System.Drawing.Size(82, 22);
            this.lb_ID.TabIndex = 2;
            this.lb_ID.Text = "ID Number";
            // 
            // tb_ID
            // 
            this.tb_ID.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_ID.Location = new System.Drawing.Point(147, 251);
            this.tb_ID.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_ID.Name = "tb_ID";
            this.tb_ID.Size = new System.Drawing.Size(557, 27);
            this.tb_ID.TabIndex = 3;
            // 
            // lb_FirstName
            // 
            this.lb_FirstName.AutoSize = true;
            this.lb_FirstName.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_FirstName.Location = new System.Drawing.Point(12, 292);
            this.lb_FirstName.Name = "lb_FirstName";
            this.lb_FirstName.Size = new System.Drawing.Size(48, 22);
            this.lb_FirstName.TabIndex = 4;
            this.lb_FirstName.Text = "Name";
            // 
            // tb_FirstName
            // 
            this.tb_FirstName.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_FirstName.Location = new System.Drawing.Point(147, 292);
            this.tb_FirstName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_FirstName.Name = "tb_FirstName";
            this.tb_FirstName.Size = new System.Drawing.Size(557, 27);
            this.tb_FirstName.TabIndex = 5;
            // 
            // lb_LastName
            // 
            this.lb_LastName.AutoSize = true;
            this.lb_LastName.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_LastName.Location = new System.Drawing.Point(12, 334);
            this.lb_LastName.Name = "lb_LastName";
            this.lb_LastName.Size = new System.Drawing.Size(71, 22);
            this.lb_LastName.TabIndex = 6;
            this.lb_LastName.Text = "Surname";
            // 
            // tb_LastName
            // 
            this.tb_LastName.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_LastName.Location = new System.Drawing.Point(147, 334);
            this.tb_LastName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_LastName.Name = "tb_LastName";
            this.tb_LastName.Size = new System.Drawing.Size(557, 27);
            this.tb_LastName.TabIndex = 7;
            // 
            // lb_Email
            // 
            this.lb_Email.AutoSize = true;
            this.lb_Email.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_Email.Location = new System.Drawing.Point(12, 377);
            this.lb_Email.Name = "lb_Email";
            this.lb_Email.Size = new System.Drawing.Size(109, 22);
            this.lb_Email.TabIndex = 8;
            this.lb_Email.Text = "Email Address";
            // 
            // tb_Email
            // 
            this.tb_Email.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_Email.Location = new System.Drawing.Point(147, 377);
            this.tb_Email.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_Email.Name = "tb_Email";
            this.tb_Email.Size = new System.Drawing.Size(557, 27);
            this.tb_Email.TabIndex = 9;
            // 
            // lb_Address
            // 
            this.lb_Address.AutoSize = true;
            this.lb_Address.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_Address.Location = new System.Drawing.Point(12, 418);
            this.lb_Address.Name = "lb_Address";
            this.lb_Address.Size = new System.Drawing.Size(128, 22);
            this.lb_Address.TabIndex = 10;
            this.lb_Address.Text = "Delivery Address";
            // 
            // tb_Address
            // 
            this.tb_Address.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_Address.Location = new System.Drawing.Point(147, 418);
            this.tb_Address.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_Address.Name = "tb_Address";
            this.tb_Address.Size = new System.Drawing.Size(557, 27);
            this.tb_Address.TabIndex = 11;
            // 
            // lb_Contact
            // 
            this.lb_Contact.AutoSize = true;
            this.lb_Contact.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_Contact.Location = new System.Drawing.Point(12, 460);
            this.lb_Contact.Name = "lb_Contact";
            this.lb_Contact.Size = new System.Drawing.Size(122, 22);
            this.lb_Contact.TabIndex = 12;
            this.lb_Contact.Text = "Contact Number";
            // 
            // tb_Contact
            // 
            this.tb_Contact.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_Contact.Location = new System.Drawing.Point(147, 460);
            this.tb_Contact.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_Contact.Name = "tb_Contact";
            this.tb_Contact.Size = new System.Drawing.Size(557, 27);
            this.tb_Contact.TabIndex = 13;
            // 
            // btn_Create
            // 
            this.btn_Create.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Create.Location = new System.Drawing.Point(529, 503);
            this.btn_Create.Name = "btn_Create";
            this.btn_Create.Size = new System.Drawing.Size(173, 46);
            this.btn_Create.TabIndex = 14;
            this.btn_Create.Text = "Create";
            this.btn_Create.UseVisualStyleBackColor = true;
            this.btn_Create.Click += new System.EventHandler(this.createButton_Click);
            // 
            // btn_Back
            // 
            this.btn_Back.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Back.Location = new System.Drawing.Point(12, 503);
            this.btn_Back.Name = "btn_Back";
            this.btn_Back.Size = new System.Drawing.Size(173, 46);
            this.btn_Back.TabIndex = 15;
            this.btn_Back.Text = "Back";
            this.btn_Back.UseVisualStyleBackColor = true;
            this.btn_Back.Click += new System.EventHandler(this.backButton_Click);
            // 
            // CreateCustomerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(715, 561);
            this.Controls.Add(this.btn_Back);
            this.Controls.Add(this.btn_Create);
            this.Controls.Add(this.tb_Contact);
            this.Controls.Add(this.lb_Contact);
            this.Controls.Add(this.tb_Address);
            this.Controls.Add(this.lb_Address);
            this.Controls.Add(this.tb_Email);
            this.Controls.Add(this.lb_Email);
            this.Controls.Add(this.tb_LastName);
            this.Controls.Add(this.lb_LastName);
            this.Controls.Add(this.tb_FirstName);
            this.Controls.Add(this.lb_FirstName);
            this.Controls.Add(this.tb_ID);
            this.Controls.Add(this.lb_ID);
            this.Controls.Add(this.lb_Customer);
            this.Controls.Add(this.img_Logo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaximizeBox = false;
            this.Name = "CreateCustomerForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Create Customer Form";
            ((System.ComponentModel.ISupportInitialize)(this.img_Logo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox img_Logo;
        private System.Windows.Forms.Label lb_Customer;
        private System.Windows.Forms.Label lb_ID;
        private System.Windows.Forms.TextBox tb_ID;
        private System.Windows.Forms.Label lb_FirstName;
        private System.Windows.Forms.TextBox tb_FirstName;
        private System.Windows.Forms.Label lb_LastName;
        private System.Windows.Forms.TextBox tb_LastName;
        private System.Windows.Forms.Label lb_Email;
        private System.Windows.Forms.TextBox tb_Email;
        private System.Windows.Forms.Label lb_Address;
        private System.Windows.Forms.TextBox tb_Address;
        private System.Windows.Forms.Label lb_Contact;
        private System.Windows.Forms.TextBox tb_Contact;
        private System.Windows.Forms.Button btn_Create;
        private System.Windows.Forms.Button btn_Back;
    }
}