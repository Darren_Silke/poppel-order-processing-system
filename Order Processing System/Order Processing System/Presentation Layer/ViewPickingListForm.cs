﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Order_Processing_System.Domain_Layer;
using Order_Processing_System.Application_Logic_Layer;
using Order_Processing_System.Database_Layer;

namespace Order_Processing_System.Presentation_Layer
{
    public partial class ViewPickingListForm : Form
    {
        private Collection<Customer> customersWithOrdersFromDatabase;
        public delegate void ReturnToMainFormButtonClickEventHandler(Button button);
        public event ReturnToMainFormButtonClickEventHandler ReturnToMainFormButtonClick;

        public ViewPickingListForm()
        {
            InitializeComponent();
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);
            if (e.CloseReason == CloseReason.WindowsShutDown) return;
            // Confirm that user wishes to exit the application.
            switch (MessageBox.Show(this, "Are you sure you want to exit the application?", "Exit Application", MessageBoxButtons.YesNo))
            {
                case DialogResult.No:
                    e.Cancel = true;
                    break;

                default:
                    break;
            }
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            OnReturnToMainFormButtonClick((Button)sender);
            this.Visible = false;
        }

        private void OnReturnToMainFormButtonClick(Button button)
        {
            if(ReturnToMainFormButtonClick != null)
            {
                this.VerticalScroll.Value = 0;
                searchTextBox.Focus();
                ReturnToMainFormButtonClick(button);
            }
        }

        private void SetupCustomerOrdersListView()
        {
            customerOrdersListView.Clear();
            customerOrdersListView.View = View.Details;

            // Set up columns of ListView. 
            customerOrdersListView.Columns.Insert(0, "Order Number", 498, HorizontalAlignment.Left);
            customerOrdersListView.Columns.Insert(1, "Customer ID Number", 498, HorizontalAlignment.Left);
            customerOrdersListView.Columns.Insert(2, "Order Status", 497, HorizontalAlignment.Left);
        }

        private void SetupOrderDetailsListView()
        {
            orderDetailsListView.Clear();
            orderDetailsListView.View = View.Details;

            // Set up columns of ListView. 
            orderDetailsListView.Columns.Insert(0, "Product Code", 298, HorizontalAlignment.Left);
            orderDetailsListView.Columns.Insert(1, "Description", 298, HorizontalAlignment.Left);
            orderDetailsListView.Columns.Insert(2, "Quantity", 298, HorizontalAlignment.Left);
            orderDetailsListView.Columns.Insert(3, "Price Per Unit (R)", 299, HorizontalAlignment.Left);
            orderDetailsListView.Columns.Insert(4, "Sub Total (R)", 300, HorizontalAlignment.Left);
        }

        private void AddInitialCustomerOrders()
        {
            customersWithOrdersFromDatabase = new CustomerController().GetCustomersWithOrders();
            ListViewItem orderDetails;
            int orderNumber = 1;

            foreach(Customer customer in customersWithOrdersFromDatabase)
            {
                orderDetails = new ListViewItem();
                orderDetails.Text = orderNumber.ToString();
                orderDetails.SubItems.Add(customer.IDNumber);
                orderDetails.SubItems.Add("In Process");

                customerOrdersListView.Items.Add(orderDetails);
                orderNumber++;
            }
        }

        private void SetupForm()
        {
            searchTextBox.Text = "";
            searchButton.Enabled = false;
            viewDetailsButton.Enabled = false;
            idNumberTextBox.Text = "";
            nameTextBox.Text = "";
            surnameTextBox.Text = "";
            emailAddressTextBox.Text = "";
            deliveryAddressTextBox.Text = "";
            contactNumberTextBox.Text = "";
            totalTextBox.Text = "";
            this.Refresh();
        }

        private void searchTextBox_TextChanged(object sender, EventArgs e)
        {
            if(searchTextBox.Text == "")
            {
                customerOrdersListView.Clear();
                SetupCustomerOrdersListView();
                orderDetailsListView.Clear();
                SetupOrderDetailsListView();
                AddInitialCustomerOrders();
                SetupForm();
            }
            else
            {
                searchButton.Enabled = true;
            }
        }

        private void searchButton_Click(object sender, EventArgs e)
        {
            idNumberTextBox.Text = "";
            nameTextBox.Text = "";
            surnameTextBox.Text = "";
            emailAddressTextBox.Text = "";
            deliveryAddressTextBox.Text = "";
            contactNumberTextBox.Text = "";
            totalTextBox.Text = "";

            orderDetailsListView.Clear();
            SetupOrderDetailsListView();

            string searchString = searchTextBox.Text;
            Dictionary<int, string> orders = new Dictionary<int,string>();
            Dictionary<int, string> newOrders = new Dictionary<int, string>();
            ListViewItem newOrderDetails;

            for(int i = 0; i < customerOrdersListView.Items.Count; i++)
            {
                int orderNumber = Convert.ToInt32(customerOrdersListView.Items[i].SubItems[0].Text);
                string customerIDNumber = customerOrdersListView.Items[i].SubItems[1].Text;

                orders.Add(orderNumber, customerIDNumber);
            }

            newOrders = Search.FindOrderMatches(searchString, orders);
            customerOrdersListView.Clear();
            SetupCustomerOrdersListView();

            foreach(KeyValuePair<int, string> newOrder in newOrders.OrderBy(orderNumber => orderNumber.Key))
            {
                newOrderDetails = new ListViewItem();
                newOrderDetails.Text = newOrder.Key.ToString();
                newOrderDetails.SubItems.Add(newOrder.Value);
                newOrderDetails.SubItems.Add("In Process");

                customerOrdersListView.Items.Add(newOrderDetails);
            }
        }

        private void ViewPickingListForm_VisibleChanged(object sender, EventArgs e)
        {
            if(this.Visible == true)
            {
                SetupForm();
                customerOrdersListView.Clear();
                orderDetailsListView.Clear();
                SetupOrderDetailsListView();
                SetupCustomerOrdersListView();
                AddInitialCustomerOrders();
            }
        }

        private void viewDetailsButton_Click(object sender, EventArgs e)
        {
            if (orderDetailsListView.Items.Count == 0)
            {
                string customerIDNumber = customerOrdersListView.SelectedItems[0].SubItems[1].Text;
                int i = 0;
                bool found = false;
                Customer customer = new Customer();
                List<Tuple<OrderItem, StockItem>> orderDetails;

                while ((i < customersWithOrdersFromDatabase.Count) && (found == false))
                {
                    if (customerIDNumber == customersWithOrdersFromDatabase[i].IDNumber)
                    {
                        customer = customersWithOrdersFromDatabase[i];
                        found = true;
                    }
                    i++;
                }

                idNumberTextBox.Text = customer.IDNumber;
                nameTextBox.Text = customer.Name;
                surnameTextBox.Text = customer.Surname;
                emailAddressTextBox.Text = customer.EmailAddress;
                deliveryAddressTextBox.Text = customer.DeliveryAddress;
                contactNumberTextBox.Text = customer.ContactNumber;

                orderDetails = new OrderItemController().GetOrderDetails(customer.IDNumber);
                OrderItem orderItem;
                StockItem stockItem;
                ListViewItem customerOrderDetails;
                decimal subtotal;
                decimal total = 0;

                for (i = 0; i < orderDetails.Count; i++)
                {
                    orderItem = orderDetails[i].Item1;
                    stockItem = orderDetails[i].Item2;

                    subtotal = orderItem.ItemQuantity * stockItem.PricePerUnit;
                    total += subtotal;

                    customerOrderDetails = new ListViewItem();
                    customerOrderDetails.Text = orderItem.ItemProductCode;
                    customerOrderDetails.SubItems.Add(stockItem.Description);
                    customerOrderDetails.SubItems.Add(orderItem.ItemQuantity.ToString());
                    customerOrderDetails.SubItems.Add(stockItem.PricePerUnit.ToString());
                    customerOrderDetails.SubItems.Add(subtotal.ToString());

                    orderDetailsListView.Items.Add(customerOrderDetails);
                }
                totalTextBox.Text = total.ToString();
            }
        }

        private void customerOrdersListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (customerOrdersListView.SelectedIndices.Count == 0)
            {
                viewDetailsButton.Enabled = false;
                SetupForm();
                orderDetailsListView.Clear();
                SetupOrderDetailsListView();
            }
            else
            {
                viewDetailsButton.Enabled = true;
            }
        }
    }
}
