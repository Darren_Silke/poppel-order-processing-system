﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using Order_Processing_System.Domain_Layer;

namespace Order_Processing_System.Database_Layer
{
    public class StockItemController : Database
    {
        private StockItem stockItem;
        private SqlDataReader reader;
        private SqlCommand command;

        public Collection<StockItem> GetStockDetails(bool fetchExpiredProducts)
        {        
            Collection<StockItem> stockItems = new Collection<StockItem>();

            try
            {
                if(fetchExpiredProducts == true)
                {
                    command = new SqlCommand("SELECT* FROM Stock WHERE expiryDate < '" + DateTime.Today + "'", connection);
                }
                else
                {
                    command = new SqlCommand("SELECT* FROM Stock WHERE expiryDate > '" + DateTime.Today.AddDays(7) + "'", connection);
                }
                // Open the connection.
                connection.Open();
                command.CommandType = CommandType.Text;
                // Read from the table.
                reader = command.ExecuteReader();
                
                if(reader.HasRows)
                {
                    while(reader.Read())
                    {
                        stockItem = new StockItem();
                        stockItem.ProductCode = reader.GetString(0);
                        stockItem.Description = reader.GetString(1);
                        stockItem.ExpiryDate = reader.GetDateTime(2);
                        stockItem.UnitsInStock = reader.GetInt32(3);
                        stockItem.PricePerUnit = reader.GetDecimal(4);
                        stockItems.Add(stockItem);
                    }
                }
            }

            catch(Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            finally
            {
                // Close the connection.
                connection.Close();
                // Close the reader.
                reader.Close();
            }
            return stockItems;
        }
    }
}
