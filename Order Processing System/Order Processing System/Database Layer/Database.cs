﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;
using Order_Processing_System.Properties;

namespace Order_Processing_System.Database_Layer
{
    public class Database
    {
        private string connectionString = Settings.Default.PoppelConnectionString;
        protected SqlConnection connection;

        public Database()
        {
            try
            {
                // Open a connection & create a new dataset object.
                connection = new SqlConnection(connectionString);
            }
            catch(Exception e)
            {
                System.Windows.Forms.MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        } 
    }
}
