﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using Order_Processing_System.Domain_Layer;

namespace Order_Processing_System.Database_Layer
{
    public class OrderItemController : Database
    {
        private SqlCommand command;
        private SqlDataReader reader;

        public bool AddOrderDetails(Collection<OrderItem> orderItems)
        {
            bool success = false;
            try
            {
                // Open the connection.
                connection.Open();
           
                foreach(OrderItem orderItem in orderItems)
                {
                    command = new SqlCommand("INSERT INTO Orders (CustomerIDNumber, ItemProductCode, ItemQuantity, OrderProcessed) VALUES('" +
                                            orderItem.CustomerIDNumber + "', '" + orderItem.ItemProductCode + "'," + orderItem.ItemQuantity + ", '" + orderItem.OrderProcessed + "')",
                                            connection);
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    success = UpdateStockDetails(orderItem.ItemProductCode, orderItem.ItemQuantity);
                }
            }
            catch(Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                success = false;
            }
            finally
            {
                connection.Close();
            }
            return success;
        }

        private bool UpdateStockDetails(string productCode, int quantity)
        {
            bool success;
            try
            {
                int unitsInStock = 0;
                command = new SqlCommand("SELECT UnitsInStock FROM Stock WHERE ProductCode = '" + productCode + "'", connection);
                command.CommandType = CommandType.Text;
                // Read from the table.
                reader = command.ExecuteReader();
                if(reader.HasRows)
                {
                    while(reader.Read())
                    {
                        unitsInStock = reader.GetInt32(0);
                    }
                }
                // Close the reader.
                reader.Close();

                int newUnitsInStock = unitsInStock - quantity;
                command = new SqlCommand("UPDATE Stock SET UnitsInStock = " + newUnitsInStock + "WHERE ProductCode = '" + productCode + "'", connection);
                command.CommandType = CommandType.Text;
                command.ExecuteNonQuery();
                success = true;
            }
            catch(Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                success = false;
            }
            finally
            {
                if(!reader.IsClosed)
                {
                    // Close the reader.
                    reader.Close();
                }
            }
            return success;
        }

        public List<Tuple<OrderItem, StockItem>> GetOrderDetails(string customerIDNumber)
        {
            List<Tuple<OrderItem, StockItem>> orderDetails = new List<Tuple<OrderItem, StockItem>>();
            
            try
            {

                OrderItem orderItem;
                StockItem stockItem;

                command = new SqlCommand("SELECT ItemProductCode, Description, ItemQuantity, PricePerUnit " + 
                                        "FROM Orders, Stock " +
                                        "WHERE ItemProductCode = ProductCode AND OrderProcessed = 'False' AND CustomerIDNumber = '" + customerIDNumber + "'", connection);

                // Open the connection.
                connection.Open();
                command.CommandType = CommandType.Text;
                // Read from the table.
                reader = command.ExecuteReader();
                if(reader.HasRows)
                {
                    while(reader.Read())
                    {
                        orderItem = new OrderItem();
                        stockItem = new StockItem();

                        orderItem.ItemProductCode = reader.GetString(0);
                        stockItem.Description = reader.GetString(1);
                        orderItem.ItemQuantity = reader.GetInt32(2);
                        stockItem.PricePerUnit = reader.GetDecimal(3);

                        orderDetails.Add(Tuple.Create(orderItem, stockItem));
                      
                    }
                }
            }
            catch(Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                // Close the connection.
                connection.Close();
                // Close the reader.
                reader.Close();
            }
            return orderDetails;
        }
    }
}
