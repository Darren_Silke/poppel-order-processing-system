﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using Order_Processing_System.Domain_Layer;

namespace Order_Processing_System.Database_Layer
{
    public class CustomerController : Database
    {
        private Customer customer;
        private SqlDataReader reader;
        private SqlCommand command;

        public Customer GetCustomerDetails(string idNumber)
        {
            customer = new Customer();

            try
            {
                command = new SqlCommand("SELECT* FROM Customers WHERE IDNumber = '" + idNumber + "'", connection);
                // Open the connection.
                connection.Open();
                command.CommandType = CommandType.Text;
                // Read from the table.
                reader = command.ExecuteReader();
                if(reader.HasRows)
                {
                    while(reader.Read())
                    {
                        customer.IDNumber = reader.GetString(0);
                        customer.Name = reader.GetString(1);
                        customer.Surname = reader.GetString(2);
                        customer.EmailAddress = reader.GetString(3);
                        customer.DeliveryAddress = reader.GetString(4);
                        customer.ContactNumber = reader.GetString(5);
                        customer.Blacklisted = reader.GetBoolean(6);
                    }
                }
            }
            catch(Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                // Close the connection.
                connection.Close();
                // Close the reader.
                reader.Close();
            }
            return customer;
        }

        public bool AddCustomerDetails(Customer customer)
        {
            bool success;
            try
            {
                command = new SqlCommand("INSERT INTO Customers(IDNumber, Name, Surname, EmailAddress, DeliveryAddress, ContactNumber, Blacklisted)"
                                        + "VALUES('" + customer.IDNumber + "', '" + customer.Name + "', '" + customer.Surname + "', '" + customer.EmailAddress + "', '"
                                        + customer.DeliveryAddress + "', '" + customer.ContactNumber + "', '" + customer.Blacklisted + "')", connection);
                // Open the connection.
                connection.Open();
                command.CommandType = CommandType.Text;
                command.ExecuteNonQuery();
                success = true;
            }
            catch(Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                success = false;
            }
            finally
            {
                connection.Close();
            }
            return success;
        }
        
        public Collection<Customer> GetCustomersWithOrders()
        { 
            Collection<Customer> customers = new Collection<Customer>();
            try
            {
                command = new SqlCommand(";WITH CTE AS (SELECT  *, RN = ROW_NUMBER() OVER(PARTITION BY CustomerIDNumber ORDER BY OrderItemID) FROM Orders, Customers WHERE CustomerIDNumber = IDNumber AND OrderProcessed = 'False' AND Blacklisted = 'False') SELECT CustomerIDNumber, Name, Surname, EmailAddress, DeliveryAddress, ContactNumber, Blacklisted FROM CTE WHERE RN = 1 ORDER BY OrderItemID", connection);
                                                                         
                // Open the connection.
                connection.Open();
                command.CommandType = CommandType.Text;
                // Read from the table.
                reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        customer = new Customer();
                        customer.IDNumber = reader.GetString(0);
                        customer.Name = reader.GetString(1);
                        customer.Surname = reader.GetString(2);
                        customer.EmailAddress = reader.GetString(3);
                        customer.DeliveryAddress = reader.GetString(4);
                        customer.ContactNumber = reader.GetString(5);
                        customer.Blacklisted = reader.GetBoolean(6);

                        customers.Add(customer);
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                // Close the connection.
                connection.Close();
                // Close the reader.
                reader.Close();
            }
            return customers;
        }
    }
}





