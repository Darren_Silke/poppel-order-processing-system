﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Order_Processing_System.Application_Logic_Layer
{
    public static class CustomerValidation
    {
        static string[] invalidChars = new string[] {  "(",")",",",".","?","`","~","!","@","#","$","%","^","&","*","[","]","-","_","\\","/","|","+","=","{","}",":",":",";","'","`","~","<",">"  };
        static string[] numbers = new string[] { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };
        static string[] vowels = new string[] { "a","e","i","o","u" };

        public static bool IDNumberValidation(string number)
        {
            if (number.Length != 13)
            {
                return false;
            }
            try
            {
                Convert.ToInt64(number);
            }
            catch (FormatException)
            {
                return false;
            }
            return true;
        }

        public static bool NameValidation(String name)
        {
            if (name.Length == 0)
            {
                return false;
            }

            for (int i = 0; i < vowels.Length; i++)
            {
                if (name.ToLower().Contains(vowels[i]))
                {
                    break;
                }
                else if (i == vowels.Length - 1)
                {
                    return false;
                }                
            }

            for (int i = 0; i < invalidChars.Length; i++)
            {
                if (name.Contains(invalidChars[i]))
                {
                    return false;
                }
            }

            for (int i = 0; i < numbers.Length; i++)
            {
                if (name.Contains(numbers[i]))
                {
                    return false;
                }
            }
                return true;            
        }
        public static bool EmailValidation(String email)
        {
            if (email.Length < 5)
            {
                return false;
            }
            if (!email.Contains("@") || !email.Contains("."))
            {
                return false;
            }
            return true;
        }

        public static bool AddressValidation(string address)
        {
            if (address.Length == 0)
            {
                return false;
            }
            return true;
        }

        public static bool ContactNumberValidation(string number)
        {
            if (number.Length != 10)
            {
                return false;
            }
            try
            {
                Convert.ToInt64(number);
            }
            catch (FormatException)
            {
                return false;
            }
            return true;
        }
    }
}