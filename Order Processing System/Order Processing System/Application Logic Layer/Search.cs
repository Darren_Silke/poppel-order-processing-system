﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Order_Processing_System.Domain_Layer;

namespace Order_Processing_System.Application_Logic_Layer
{
    public static class Search
    {            
        public static ArrayList FindStockItemMatches(string searchString, ArrayList stockItems)
        {
            ArrayList newStockItems = new ArrayList();

            foreach(StockItem stockItem in stockItems)
            {
                if(stockItem.Description.ToLower().Contains(searchString.ToLower()))
                {
                    newStockItems.Add(stockItem);
                }
            }
            return newStockItems;
        }

        public static Dictionary<int, string> FindOrderMatches(string searchString, Dictionary<int, string> orders)
        {
            Dictionary<int, string> newOrders = new Dictionary<int, string>();

            for(int i = 0; i < orders.Count; i++)
            {
                if (orders.ElementAt(i).Value.Contains(searchString))
                {
                    int orderNumber = orders.ElementAt(i).Key;
                    string customerIDNumber = orders.ElementAt(i).Value;
                    newOrders.Add(orderNumber, customerIDNumber);
                }
            }
            return newOrders;
        }
    }
}
