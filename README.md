# README #

### What Is This Repository For? ###

* Quick summary

This software implements a segment of the Poppel Order Processing System. The software makes use of a relational database which is built, populated and maintained. The software provides the following basic functionality with the addition of extra features:

1. Create a customer.
2. Create a customer order for at least 3 products (including checking if customer is blacklisted & checking of inventory). Inventory items are able to be reserved for a customer.
3. Cancel an item that is not invoiced as yet.
4. Generate picking list to initiate delivery.
5. Print report to identify all expired products in inventory. 

Products will already be present in the database. The software will not have the functionality to create new products.

N.B. IN ORDER TO UNDERSTAND THE EXACT CONTEXT OF THE SYSTEM, IT IS BEST TO READ THE POPPEL CASE STUDY AND SYSTEM SPECIFICATION DOCUMENT.

* Version

Current version is 1.0.

### How Do I Get Set Up? ###

* Dependencies

In order to run the software, Microsoft Visual Studio 2013 is required. 

* Summary of set up

Open the Microsoft Visual Studio Solution project file. Once opened, first build the project and ensure that a connection to the Poppel.mdf database file is present. Once all files have been compiled, run the software in Visual Studio.

* Database configuration

To ensure that new data added to the system through the use of the application is preserved in the database the next time the application is run, change the Poppel.mdf property of 'Copy to Output Directory' from 'Copy always' to 'Do not copy'. 

* How To Run Tests

Interact with the application through the forms and evaluate output and compare it with the expected output. 

### Who Do I Talk To? ###

* Repo owner or admin --> Darren Silke (d.silke@telkomsa.net) & Matthew du Toit (r0guehunt3r@gmail.com)